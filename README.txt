
== Introduction ==

The OData Server module exposes entities, like nodes and users, through the
OData Producer Library for PHP. Properties and core fields are supported,
and the module can be configured to limit the bundles, properties, and fields
that are exposed through the server. Hooks are provided for developers to add
support for their custom field modules. Most of the read-only conventions of
the OData specification are supported.

Sponsored by R2integrated.
http://www.r2integrated.com

== Dependencies ==

The OData Server module depends on the Libraries module. You will also need to
download the OData Producer Library for PHP and place this in the
sites/(all|default)/libraries directory. This module requires PHP >= 5.3.

== Installation and Usage ==

1. Download and install the Libraries (2.0 Branch) module:
   http://drupal.org/project/libraries

2. Download the OData Producer Library for PHP:
   https://github.com/MSOpenTech/odataphpprod/

3. Extract the library to sites/all/libraries or sites/default/libraries,
   creating the libraries directory if it does not already exist.

4. Ensure correct file placement - Index.php in the library should be located
   at %root%/sites/all/libraries/odata/Index.php

5. Install the OData Server module (this module).

6. Go to the admin settings page to configure which bundles, properties, and
   fields to expose.
   admin/config/development/odata

7. Browse to odata.svc (http://example.com/path/to/drupal/odata.svc). Your
   browser may or may not show the XML/JSON by default, but you should be able
   to see something similar to the following by viewing the page source:

<service>
  <workspace>
    <atom:title>Default</atom:title>
    <collection href="Node__News">
      <atom:title>Node__News</atom:title>
    </collection>
    <collection href="Taxonomy_term__Tags">
      <atom:title>Taxonomy_term__Tags</atom:title>
    </collection>
    <collection href="Taxonomy_vocabulary__Taxonomy_vocabulary">
      <atom:title>Taxonomy_vocabulary__Taxonomy_vocabulary</atom:title>
    </collection>
    <collection href="User__User">
      <atom:title>User__User</atom:title>
    </collection>
  </workspace>
</service>

7. The preceeding sample response contains a list of entity-bundles that
   are available to query. To return a list of all nodes of the "news" content
   type, you would construct the following URL, taking note of the double
   underscore (__) separating the entity type and the bundle name:
   http://example.com/path/to/drupal/odata.svc/Node__News

== Security ==

Please be aware that this module can be configured to expose all database
properties of entities. This includes the user entity's password hash field.
Rather than arbitrarily decide that this module would not expose this field,
the author suggests that it is the responsibility of the site administrator
to take appropriate measures to secure the web service if he or she chooses
to expose sensitive properties or fields. To this end, the module will
identify certain fields as potential security risks.

== Known Issues ==

Due to how comment entities work in D7, they will not work properly in the
OData server. See http://drupal.org/node/938462 for more details. The author
is open to any ideas to provide partial or full support for comment entities
in the future.
