<?php

/**
 * @file
 * Dispatches OData requests to the OData SDK.
 */

namespace ODataServer;

use ODataProducer\OperationContext\DataServiceHost;
use ODataProducer\OperationContext\Web\OutgoingResponse;
use ODataServer\Utils\ClassAutoLoader;
use ODataServer\Service\DrupalDataService;
use ODataServer\Providers\DrupalMetadataProvider;
use ODataServer\Utils\Singleton;
// Need to get to this before autoloader is registered.
require_once drupal_get_path('module', 'odata_server') . '/classes/ODataServer/Utils/Singleton.php';

class ODataServer extends Singleton {

  /**
   * Store OData SDK library info from the libraries module.
   *
   * @var Array
   */
  protected $library;

  /**
   * Initializes the OData SDK.
   */
  public function __construct() {
    $this->library = libraries_load('odata');
    if (isset($library['error'])) {
      drupal_set_message($library['error message'], 'error');
      drupal_goto();
    }
    $this->loadSdk($this->library['library path']);
    $this->declareEntityClasses();
  }

  /**
   * Load the OData SDK and prepare the autoload callback.
   *
   * @param string $path
   *   The path to the OData SDK.
   */
  protected function loadSdk($path) {
    require_once drupal_get_path('module', 'odata_server') . '/classes/ODataServer/Utils/ClassAutoLoader.php';
    $library = libraries_load('odata');
    ClassAutoLoader::register(array(
      drupal_get_path('module', 'odata_server') . '/classes',
      $library['library path'] . '/library',
    ));
  }

  /**
   * Constructs metadata used by the SDK.
   */
  protected function declareEntityClasses() {
    $entities = DrupalMetadataProvider::getEntities();
    ob_start();
    include drupal_get_path('module', 'odata_server') . '/classes/entities_template.inc';
    $rendered = ob_get_clean();
    if (eval($rendered) === FALSE) {
      drupal_set_message('Unable to declare entities.', 'error');
      drupal_goto();
    }
  }

  /**
   * Have the OData SDK handle the request.
   * @static
   */
  public static function route() {
    self::getInstance();
    $data_service = new DrupalDataService();
    $data_service_host = new DataServiceHost();
    $data_service_host->setAbsoluteServiceUri('/odata.svc');
    $data_service->setHost($data_service_host);
    $data_service->handleRequest();
    $response = $data_service->getHost()->getWebOperationContext()->outgoingResponse();
    foreach ($response->getHeaders() as $header_name => $header_value) {
      if (!is_null($header_value)) {
        header($header_name . ':' . $header_value);
      }
    }
    echo $response->getStream();
  }

}
