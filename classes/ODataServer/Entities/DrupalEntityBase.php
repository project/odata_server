<?php

/**
 * @file
 * Provides base methods for metadata generated in ODataServer.
 */

namespace ODataServer\Entities;

use ODataProducer\Providers\Metadata\ServiceBaseMetadata;
use ODataProducer\Providers\Metadata\ResourceType;
use ODataProducer\Providers\Metadata\Type\EdmPrimitiveType;
use ReflectionClass;

abstract class DrupalEntityBase {

  /**
   * Returns an array of properties belonging to the entity subclass.
   */
  public static function getProperties() {
    return static::$properties;
  }

  /**
   * Returns the base table used by the entity.
   */
  public static function getBaseTable() {
    return static::$base_table;
  }

  /**
   * Returns the entity's primary key.
   */
  public static function getPrimaryKey() {
    return static::$primary_key;
  }

}
