<?php

/**
 * @file
 * Subclass of EntityFieldQuery. Provides support for where clauses with
 * arbitrary expressions for properties. In the future, we may support
 * expressions on fields, and also OR conditionals.
 */

namespace ODataServer\Entities;

use \EntityFieldQuery;
use \EntityFieldQueryException;
use ODataServer\Providers\DrupalMetadataProvider;

class ODataEntityFieldQuery extends EntityFieldQuery {

  /**
   * @var array
   */
  public $whereExpressions = array();

  /**
   * Add an expression-based where clause.
   *
   * @param array $expression
   *   The expression.
   *
   * @return ODataEntityFieldQuery
   *   The query.
   */
  public function addWhereExpression($expression) {
    $this->whereExpressions[] = $expression;
    $this->addTag('odata_server_where_expressions');
    return $this;
  }

}
