<?php

/**
 * @file
 * Provides base functionality for complex fields. Complex fields are fields
 * that have a non-ptimitive value and are not references to other entities. An
 * example would be a date field with a start and end date - effectively two
 * values. At the moment, only primitive data types are supported for complex
 * fields.
 */

namespace ODataServer\ObjectModel;

abstract class ComplexPropertyBase {

  /**
   * @var array
   */
  protected static $properties = array();

  /**
   * Returns an array mapping properties to data types.
   *
   * @abstract
   * @return array
   *   The property/type mapping array.
   */
  public final static function getProperties() {
    return static::$properties;
  }

}
