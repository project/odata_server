<?php

/**
 * @file
 * Represents a non-primitive date field value. In this case, a date field with
 * a start and an end date.
 */

namespace ODataServer\ObjectModel;

use \DateTime;

class Date extends ComplexPropertyBase {

  /**
   * @var string
   */
  public $value;

  /**
   * @var string
   */
  public $value2;

  /**
   * @var array
   */
  protected static $properties = array(
    'value' => 'EdmPrimitiveType::DATETIME',
    'value2' => 'EdmPrimitiveType::DATETIME',
  );

  /**
   * Transforms raw values from Drupal to appropriate OData types.
   *
   * @static
   *
   * @param array $items
   *   Raw array of values.
   * @param int $cardinality
   *   Field cardinality.
   * @param bool $timestamp
   *   Whether or not the supplied date values are Unix timestamps.
   *
   * @return Date[]|Date
   *   The date or array of dates.
   */
  public static function dateValueCallback($items, $cardinality = 1, $timestamp = FALSE) {
    if ($cardinality != 1) {
      $return = array();
      foreach ($items as $item) {
        $instance = new self();
        if (intval($item['value']) == 0) {
          $instance->value = NULL;
        }
        else {
          if ($timestamp) {
            $datetime = new DateTime();
            $datetime->setTimestamp($item['value']);
            $instance->value = $datetime->format('c');
          }
          else {
            $instance->value = $item['value'];
          }
        }
        if (intval($item['value2']) == 0) {
          $instance->value2 = NULL;
        }
        else {

          if ($timestamp) {
            $datetime = new DateTime();
            $datetime->setTimestamp($item['value2']);
            $instance->value2 = $datetime->format('c');
          }
          else {
            $instance->value2 = $item['value2'];
          }
        }
        $return[] = $instance;
      }
      return $return;
    }
    $instance = new self();
    if (intval($items[0]['value']) == 0) {
      $instance->value = NULL;
    }
    else {
      if ($timestamp) {
        $datetime = new DateTime();
        $datetime->setTimestamp($items[0]['value']);
        $instance->value = $datetime->format('c');
      }
      else {
        $instance->value = $items[0]['value'];
      }
    }
    if (intval($items[0]['value2']) == 0) {
      $instance->value2 = NULL;
    }
    else {
      if ($timestamp) {
        $datetime = new DateTime();
        $datetime->setTimestamp($items[0]['value2']);
        $instance->value2 = $datetime->format('c');
      }
      else {
        $instance->value2 = $items[0]['value2'];
      }
    }
    return $instance;
  }

  /**
   * Invoke dateValueCallback() and return UNIX timestamp.
   *
   * @static
   *
   * @param array $items
   *   Raw array of values.
   * @param int $cardinality
   *   Field cardinality.
   *
   * @return Date|Date[]
   *   The date or array of dates.
   */
  public static function datestampValueCallback($items, $cardinality = 1) {
    return self::dateValueCallback($items, $cardinality, TRUE);
  }
}
