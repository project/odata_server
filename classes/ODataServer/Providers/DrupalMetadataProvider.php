<?php

/**
 * @file
 * Builds tree of entities, their bundles, and their fields, properties, and
 * foreign keys.
 */

namespace ODataServer\Providers;

use ODataServer\Providers\Metadata\Entity;
use ODataServer\Providers\Metadata\Bundle;
use ODataServer\Providers\Metadata\ForeignKey;
use ODataServer\Providers\Metadata\Property;
use ODataServer\Providers\Metadata\Field;
use ODataServer\Providers\Metadata\BackReference;
use ODataServer\Utils\Singleton;
use ReflectionClass;

class DrupalMetadataProvider extends Singleton {

  /**
   * @var Entity[] Collection of entities defined in Drupal.
   */
  protected $entities = array();

  /**
   * @var bool
   */
  protected $isInitialized = FALSE;

  /**
   * Returns whether or not the entity collection has been built.
   *
   * @static
   * @return bool
   *   Whether or not the entity collection has been built.
   */
  public static function isInitialized() {
    return self::getInstance()->isInitialized;
  }

  /**
   * Build the entity collection.
   * @static
   */
  protected static function init() {
    self::getInstance()->isInitialized = TRUE;
    $instances = field_info_instances();
    $fields = field_info_fields();
    $odata_field_info = odata_server_get_odata_field_info();
    $odata_property_info = odata_server_get_odata_property_info();
    $foreign_keys = array();
    foreach ($instances as $entity_name => $entity) {
      $entity_info = entity_get_info($entity_name);
      $bundle_info = field_info_bundles($entity_name);
      $base_table = $entity_info['base table'];
      $schema = drupal_get_schema($base_table);
      $primary_key = $entity_info['entity keys']['id'];
      $entity_obj = new Entity();
      $entity_obj->setName($entity_name);
      $entity_obj->setLabel($entity_info['label']);
      $entity_obj->setBaseTable($base_table);
      $entity_obj->setPrimaryKey($primary_key);
      foreach ($entity as $bundle_name => $target_bundle) {
        if (isset($bundle_info[$bundle_name])) {
          $property_permissions = variable_get('odataserver-permissions-' . $entity_name . '__' . $bundle_name . '::properties', array());
          $variable_name = 'odataserver-permissions-' . $entity_name . '__' . $bundle_name;
          $bundle_obj = new Bundle();
          $bundle_obj->setName($bundle_name);
          $bundle_obj->setLabel($bundle_info[$bundle_name]['label']);
          $bundle_obj->setEnabled(variable_get($variable_name, FALSE));
          $entity_obj->addBundle($bundle_obj);
          foreach ($schema['fields'] as $property_name => $property) {
            $variable_name = 'odataserver-permissions-' . $entity_name . '__' . $bundle_name . '::properties::' . $property_name;
            $type = FALSE;
            $value_callback = FALSE;
            $expression_callback = FALSE;
            $description = FALSE;
            if (isset($odata_property_info[$entity_name][$property_name])) {
              $type = $odata_property_info[$entity_name][$property_name]['type'];
              if (isset($odata_property_info[$entity_name][$property_name]['value callback'])) {
                $value_callback = $odata_property_info[$entity_name][$property_name]['value callback'];
              }
              if (isset($odata_property_info[$entity_name][$property_name]['expression callback'])) {
                $expression_callback = $odata_property_info[$entity_name][$property_name]['expression callback'];
              }
              if (isset($odata_property_info[$entity_name][$property_name]['description'])) {
                $description = $odata_property_info[$entity_name][$property_name]['description'];
              }
            }
            else {
              $ref_class = new ReflectionClass('ODataServer\Providers\Metadata\DrupalSchemaFieldType');
              $types = array();
              if ($ref_class) {
                $types = $ref_class->getConstants();
              }
              $type = $types[strtoupper($property['type'])];
            }
            if ($type) {
              $property_obj = new Property();
              $property_obj->setName($property_name);
              $property_obj->setSchemaType($property['type']);
              $property_obj->setEdmType($type);
              if (isset($property_permissions[$property_name])) {
                $property_obj->setEnabled(!empty($property_permissions[$property_name]));
              }
              if ($description) {
                $property_obj->setDescription($description);
              }
              if ($value_callback) {
                $property_obj->setValueCallback($value_callback);
              }
              if ($expression_callback) {
                $property_obj->setExpressionCallback($expression_callback);
              }
              $bundle_obj->addProperty($property_obj);
            }
          }
          if (isset($schema['foreign keys'])) {
            foreach ($schema['foreign keys'] as $key) {
              $foreign_keys[$entity_obj->getName() . '::' . $bundle_obj->getName()] = array(
                'key' => array_pop($key['columns']),
                'table' => $key['table'],
                'entity' => $entity_obj,
                'bundle' => $bundle_obj,
              );
            }
          }
          $field_permissions = variable_get('odataserver-permissions-' . $entity_name . '__' . $bundle_name . '::fields', array());
          foreach ($target_bundle as $field_instance) {
            if (!$field_instance['deleted']) {
              $field_metadata = $fields[$field_instance['field_name']];
              $field_obj = new Field();
              $field_obj->setId($field_instance['field_id']);
              $field_obj->setName($field_instance['field_name']);
              $field_obj->setDrupalType($field_metadata['type']);
              $field_obj->setModule($field_metadata['module']);
              $field_obj->setCardinality($field_metadata['cardinality']);
              if (isset($field_permissions[$field_obj->getName()])) {
                $field_obj->setEnabled(!empty($field_permissions[$field_obj->getName()]));
              }
              if (isset($odata_field_info[$field_obj->getModule()])
                && isset($odata_field_info[$field_obj->getModule()]['types'][$field_obj->getDrupalType()])) {
                $info = $odata_field_info[$field_obj->getModule()]['types'][$field_obj->getDrupalType()];
                $field_obj->setPrimitive($info['primitive']);
                $field_obj->setEdmType($info['type']);
                if (isset($info['complex'])) {
                  $field_obj->setComplex($info['complex']);
                }
                if (isset($info['value callback']) && function_exists($info['value callback'])) {
                  $field_obj->setValueCallback($info['value callback']);
                }
                if (isset($info['settings callback']) && function_exists($info['settings callback'])) {
                  $field_obj->setSettingsCallback($info['settings callback']);
                  $callback = $field_obj->getSettingsCallback();
                  $callback($field_obj);
                }
                $bundle_obj->addField($field_obj);
              }
            }
          }
        }
      }
      self::addEntity($entity_obj);
    }
    foreach ($foreign_keys as $foreign_key) {
      if ($target_entity = self::getEntityByBaseTable($foreign_key['table'])) {
        $source_entity = $foreign_key['entity'];
        if ($property = $foreign_key['bundle']->getPropertyByName($target_entity->getPrimaryKey())) {
          $foreign_key_permissions = variable_get('odataserver-permissions-' . $source_entity->getName() . '__' . $foreign_key['bundle']->getName() . '::foreign_keys', array());
          $foreign_key_obj = new ForeignKey();
          $foreign_key_obj->setKey($target_entity->getPrimaryKey());
          if (isset($foreign_key_permissions[$foreign_key_obj->getKey()])) {
            $foreign_key_obj->setEnabled(!empty($foreign_key_permissions[$foreign_key_obj->getKey()]));
          }
          $bundles = $target_entity->getBundles();
          $target_bundle = array_pop($bundles);
          $foreign_key_obj->setBundleName($target_bundle->getName());
          $foreign_key_obj->setEntityName($target_entity->getName());
          $foreign_key['bundle']->addForeignKey($foreign_key_obj);

          $back_reference = new BackReference();
          $back_reference->setSourceEntityName($target_entity->getName());
          $back_reference->setSourceBundleName($target_bundle->getName());
          $back_reference->setTargetEntityName($source_entity->getName());
          $back_reference->setTargetBundleName($foreign_key['bundle']->getName());
          $back_reference->setTargetForeignKey($target_entity->getPrimaryKey());
          if (isset($foreign_key_permissions[$foreign_key_obj->getKey()])) {
            $back_reference->setEnabled(!empty($foreign_key_permissions[$foreign_key_obj->getKey()]));
          }

          $target_bundle->addBackReference($back_reference);
        }
      }
    }
  }

  /**
   * Add an entity.
   * @static
   *
   * @param Entity $entity
   *   The entity to be added.
   */
  public static function addEntity(Entity $entity) {
    array_push(self::getInstance()->entities, $entity);
  }

  /**
   * Returns entire entity collection.
   *
   * @return array|Entity[]
   *   The entire entity collection.
   */
  public static function getEntities() {
    if (!self::isInitialized()) {
      self::init();
    }
    return self::getInstance()->entities;
  }

  /**
   * Get an entity from the collection by the entity's name.
   * @static
   *
   * @param string $name
   *   The name of the entity to find.
   *
   * @return null|Entity
   *   The entity or null if not found.
   */
  public static function getEntityByName($name) {
    foreach (self::getEntities() as $entity) {
      if ($entity->getName() === $name) {
        return $entity;
      }
    }
    return NULL;
  }

  /**
   * Get an entity from the collection by the entity's base table.
   * @static
   *
   * @param string $base_table
   *   The base table of the entity to find.
   *
   * @return null|Entity
   *   The entity or null if not found.
   */
  public static function getEntityByBaseTable($base_table) {
    foreach (self::getEntities() as $entity) {
      if ($entity->getBaseTable() === $base_table) {
        return $entity;
      }
    }
    return NULL;
  }

}
