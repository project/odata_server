<?php

/**
 * @file
 * This represents a foreign key reference to another entity. These are
 * determined in DrupalMetadataProvider and are created based on foreign keys
 * listed in hook_schema implementations.
 */

namespace ODataServer\Providers\Metadata;

class BackReference {

  /**
   * @var string
   */
  protected $sourceEntityName;

  /**
   * @var string
   */
  protected $sourceBundleName;

  /**
   * @var string
   */
  protected $targetEntityName;

  /**
   * @var string
   */
  protected $targetBundleName;

  /**
   * @var string
   */
  protected $targetForeignKey;

  /**
   * @var bool
   */
  protected $enabled;

  /**
   * Set the source bundle name.
   *
   * @param string $source_bundle_name
   *   The source bundle name.
   */
  public function setSourceBundleName($source_bundle_name) {
    $this->sourceBundleName = $source_bundle_name;
  }

  /**
   * Returns the source bundle name.
   *
   * @return string
   *   The source bundle name.
   */
  public function getSourceBundleName() {
    return $this->sourceBundleName;
  }

  /**
   * Set the source entity name.
   *
   * @param string $source_entity_name
   *   The source entity name.
   */
  public function setSourceEntityName($source_entity_name) {
    $this->sourceEntityName = $source_entity_name;
  }

  /**
   * Returns the source entity name.
   *
   * @return string
   *   The source entity name.
   */
  public function getSourceEntityName() {
    return $this->sourceEntityName;
  }

  /**
   * Set the target bundle name.
   *
   * @param string $target_bundle_name
   *   The target bundle name.
   */
  public function setTargetBundleName($target_bundle_name) {
    $this->targetBundleName = $target_bundle_name;
  }

  /**
   * Returns the target bundle name.
   *
   * @return string
   *   The target bundle name.
   */
  public function getTargetBundleName() {
    return $this->targetBundleName;
  }

  /**
   * Set the target entity name.
   *
   * @param string $target_entity_name
   *   The target entity name.
   */
  public function setTargetEntityName($target_entity_name) {
    $this->targetEntityName = $target_entity_name;
  }

  /**
   * Returns the target entity name.
   * @return string
   *   The target entity name.
   */
  public function getTargetEntityName() {
    return $this->targetEntityName;
  }

  /**
   * Set the target foreign key.
   *
   * @param string $target_foreign_key
   *   The target foreign key.
   */
  public function setTargetForeignKey($target_foreign_key) {
    $this->targetForeignKey = $target_foreign_key;
  }

  /**
   * Returns the target foreign key.
   *
   * @return string
   *   The target foreign key.
   */
  public function getTargetForeignKey() {
    return $this->targetForeignKey;
  }

  /**
   * Set whether to expose this foreign key through the SDK.
   *
   * @param boolean $enabled
   *   Whether to expose this foreign key through the SDK.
   */
  public function setEnabled($enabled) {
    $this->enabled = $enabled;
  }

  /**
   * Returns whether to expose this foreign key through the SDK.
   *
   * @return boolean
   *   Whether to expose this foreign key through the SDK.
   */
  public function getEnabled() {
    return $this->enabled;
  }
}
