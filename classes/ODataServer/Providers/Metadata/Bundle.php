<?php

/**
 * @file
 * Represents a bundle in Drupal. Bundles in Drupal don't technically have
 * properties or foreign keys, however it's useful for the OData SDK to treat
 * bundles as if they had properties and foreign keys of their base entities.
 */

namespace ODataServer\Providers\Metadata;

use ODataServer\Providers\Metadata\Entity;
use ODataServer\Providers\Metadata\Field;

class Bundle {

  /**
   * @var string
   */
  protected $name;

  /**
   * @var string
   */
  protected $label;

  /**
   * @var bool
   */
  protected $enabled;

  /**
   * @var Property[]
   */
  protected $properties = array();

  /**
   * @var ForeignKey[]
   */
  protected $foreignKeys = array();

  /**
   * @var BackReference[]
   */
  protected $backReferences = array();

  /**
   * @var Field[]
   */
  protected $fields = array();

  /**
   * Returns the entire collection of fields.
   *
   * @return Field[]
   *   The entire collection of fields.
   */
  public function getFields() {
    return $this->fields;
  }

  /**
   * Add a field to the collection.
   *
   * @param Field $field
   *   The field to add to the collection.
   */
  public function addField(Field $field) {
    array_push($this->fields, $field);
  }

  /**
   * Find a field by the field's name.
   *
   * @param string $name
   *   The name of the field to find.
   *
   * @return null|Field
   *   Return the field or null if not found.
   */
  public function getFieldByName($name) {
    foreach ($this->getFields() as $field) {
      if ($field->getName() === $name) {
        return $field;
      }
    }
    return NULL;
  }

  /**
   * Set the bundle's name.
   *
   * @param string $name
   *   The bundle's name.
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * Get the bundle's name.
   *
   * @return string
   *   The bundle's name.
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Set the bundle's label.
   *
   * @param string $label
   *   The bundle's label.
   */
  public function setLabel($label) {
    $this->label = $label;
  }

  /**
   * Get the bundle's label.
   *
   * @return string
   *   The bundle's label.
   */
  public function getLabel() {
    return $this->label;
  }

  /**
   * Set whether or not the bundle should be exposed by the service.
   *
   * @param boolean $enabled
   *   Whether or not the bundle should be exposed by the service.
   */
  public function setEnabled($enabled) {
    $this->enabled = $enabled;
  }

  /**
   * Get whether or not the bundle should be exposed by the service.
   *
   * @return boolean
   *   Whether or not the bundle should be exposed by the service.
   */
  public function getEnabled() {
    return $this->enabled;
  }

  /**
   * Returns the entire collection of properties.
   * @return Property[]
   *   The entire collection of properties.
   */
  public function getProperties() {
    return $this->properties;
  }

  /**
   * Add a property to the collection.
   *
   * @param Property $property
   *   The property to be added.
   */
  public function addProperty(Property $property) {
    array_push($this->properties, $property);
  }

  /**
   * Find a property by its name.
   *
   * @param string $name
   *   The name of the property to find.
   *
   * @return null|Property
   *   The property or null if not found.
   */
  public function getPropertyByName($name) {
    foreach ($this->getProperties() as $property) {
      if ($property->getName() === $name) {
        return $property;
      }
    }
    return NULL;
  }

  /**
   * Returns the entire collection of foreign keys.
   * @return array|ForeignKey[]
   *   The entire collection of foreign keys.
   */
  public function getForeignKeys() {
    return $this->foreignKeys;
  }

  /**
   * Add a foreign key to the collection.
   *
   * @param ForeignKey $foreign_key
   *   The foreign key to add.
   */
  public function addForeignKey(ForeignKey $foreign_key) {
    array_push($this->foreignKeys, $foreign_key);
  }

  /**
   * Find a foreign key by its key.
   *
   * @param string $key
   *   The key of the foreign key to find.
   *
   * @return null|ForeignKey
   *   The foreign key or null if not found.
   */
  public function getForeignKeyByKey($key) {
    foreach ($this->getForeignKeys() as $foreign_key) {
      if ($foreign_key->getKey() === $key) {
        return $foreign_key;
      }
    }
    return NULL;
  }

  /**
   * Returns the entire collection of backreferences.
   * @return array|BackReference[]
   *   The entire collection of backreferences.
   */
  public function getBackReferences() {
    return $this->backReferences;
  }

  /**
   * Add a backreference to the collection.
   *
   * @param BackReference $back_reference
   *   The backreference to add.
   */
  public function addBackReference(BackReference $back_reference) {
    array_push($this->backReferences, $back_reference);
  }
}
