<?php

/**
 * @file
 * Contains a mapping of Drupal schema field data types to EDM types used in
 * the OData SDK.
 */

namespace ODataServer\Providers\Metadata;

class DrupalSchemaFieldType {

  const CHAR = 'EdmPrimitiveType::STRING';
  const VARCHAR = 'EdmPrimitiveType::STRING';
  const TEXT = 'EdmPrimitiveType::STRING';
  const BLOB = 'EdmPrimitiveType::BINARY';
  const INT = 'EdmPrimitiveType::INT32';
  const FLOAT = 'EdmPrimitiveType::DOUBLE';
  const NUMERIC = 'EdmPrimitiveType::DECIMAL';
  const SERIAL = 'EdmPrimitiveType::INT32';

}
