<?php

/**
 * @file
 * Represents a Drupal entity.
 */

namespace ODataServer\Providers\Metadata;

use ODataServer\Providers\Metadata\Bundle;
use ODataServer\Providers\Metadata\ForeignKey;
use ODataServer\Providers\Metadata\Property;

class Entity {

  /**
   * @var string
   */
  protected $name;

  /**
   * @var string
   */
  protected $label;

  /**
   * @var string
   */
  protected $baseTable;

  /**
   * @var string
   */
  protected $primaryKey;

  /**
   * @var Bundle[]
   */
  protected $bundles = array();

  /**
   * Set the entity's base table.
   *
   * @param string $base_table
   *   The base table.
   */
  public function setBaseTable($base_table) {
    $this->baseTable = $base_table;
  }

  /**
   * Returns the base table.
   *
   * @return string
   *   The base table.
   */
  public function getBaseTable() {
    return $this->baseTable;
  }

  /**
   * Set name of the entity.
   *
   * @param string $name
   *   The name of the entity.
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * Returns the name of the entity.
   *
   * @return string
   *   The name of the entity.
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Set the entity's label.
   *
   * @param string $label
   *   The entity's label.
   */
  public function setLabel($label) {
    $this->label = $label;
  }

  /**
   * Returns the entity's label.
   *
   * @return string
   *   The entity's label.
   */
  public function getLabel() {
    return $this->label;
  }

  /**
   * Set the primary key.
   *
   * @param string $primary_key
   *   The primary key.
   */
  public function setPrimaryKey($primary_key) {
    $this->primaryKey = $primary_key;
  }

  /**
   * Returns the primary key.
   *
   * @return string
   *   The primary key.
   */
  public function getPrimaryKey() {
    return $this->primaryKey;
  }

  /**
   * Returns the entire collection of bundles.
   *
   * @return array|Bundle[]
   *   The entire collection of bundles.
   */
  public function getBundles() {
    return $this->bundles;
  }

  /**
   * Add a bundle to the collection.
   *
   * @param Bundle $bundle
   *   The bundle to add.
   */
  public function addBundle(Bundle $bundle) {
    array_push($this->bundles, $bundle);
  }

  /**
   * Return a bundle by its name.
   *
   * @param string $name
   *   The bundle's name.
   *
   * @return null|Bundle
   *   The bundle or null if not found.
   */
  public function getBundleByName($name) {
    foreach ($this->getBundles() as $bundle) {
      if (strtolower($bundle->getName()) === strtolower($name)) {
        return $bundle;
      }
    }
    return NULL;
  }

}
