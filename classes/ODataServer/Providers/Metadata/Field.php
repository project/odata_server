<?php

/**
 * @file
 * Represents a FieldAPI field instance in Drupal.
 */

namespace ODataServer\Providers\Metadata;

class Field {

  /**
   * @var string
   */
  protected $name;

  /**
   * @var string
   */
  protected $drupalType;

  /**
   * @var int
   */
  protected $id;

  /**
   * @var string
   */
  protected $module;

  /**
   * @var int
   */
  protected $cardinality;

  /**
   * @var string
   */
  protected $settingsCallback;

  /**
   * @var bool
   */
  protected $primitive;

  /**
   * @var bool
   */
  protected $complex;

  /**
   * @var string
   */
  protected $complexClass;

  /**
   * @var string
   */
  protected $edmType;

  /**
   * @var string
   */
  protected $referenceType;

  /**
   * @var string
   */
  protected $valueCallback;

  /**
   * @var bool
   */
  protected $enabled;

  /**
   * Set the cardinality of this field instance.
   *
   * @param int $cardinality
   *   The cardinality level.
   */
  public function setCardinality($cardinality) {
    $this->cardinality = intval($cardinality);
  }

  /**
   * Returns the cardinality level.
   *
   * @return int
   *   The cardinality level.
   */
  public function getCardinality() {
    return $this->cardinality;
  }

  /**
   * Set Drupal-defined Field API type.
   *
   * @param string $drupal_type
   *   The Field API type.
   */
  public function setDrupalType($drupal_type) {
    $this->drupalType = $drupal_type;
  }

  /**
   * Returns the Field API type.
   *
   * @return string
   *   The Field API type.
   */
  public function getDrupalType() {
    return $this->drupalType;
  }

  /**
   * Set the field ID.
   *
   * @param int $id
   *   The id.
   */
  public function setId($id) {
    $this->id = intval($id);
  }

  /**
   * Returns the id.
   *
   * @return int
   *   The id.
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Set the field's defining module.
   *
   * @param string $module
   *   The defining module.
   */
  public function setModule($module) {
    $this->module = $module;
  }

  /**
   * Returns the defining module.
   *
   * @return string
   *   The defining module.
   */
  public function getModule() {
    return $this->module;
  }

  /**
   * Set the field name.
   *
   * @param string $name
   *   The field name.
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * Returns the field name.
   *
   * @return string
   *   The field name.
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Set whether or not the field should be treated as a primitive datatype.
   *
   * @param boolean $primitive
   *   Whether or not field is primitive.
   */
  public function setPrimitive($primitive) {
    $this->primitive = $primitive;
  }

  /**
   * Returns whether or not field is primitive.
   *
   * @return boolean
   *   Whether or not field is primitive.
   */
  public function getPrimitive() {
    return $this->primitive;
  }

  /**
   * Set the callback used to alter the field properties.
   *
   * @param string $settings_callback
   *   The callback.
   */
  public function setSettingsCallback($settings_callback) {
    $this->settingsCallback = $settings_callback;
  }

  /**
   * Returns the callback.
   *
   * @return string
   *   The callback.
   */
  public function getSettingsCallback() {
    return $this->settingsCallback;
  }

  /**
   * If primitive, this is the primitive type used by the SDK.
   *
   * @param string $edm_type
   *   The primitive type.
   */
  public function setEdmType($edm_type) {
    $this->edmType = $edm_type;
  }

  /**
   * Returns the primitive type.
   *
   * @return string
   *   The primitive type.
   */
  public function getEdmType() {
    return $this->edmType;
  }

  /**
   * Set the callback used to prepare the value for consumption by the SDK.
   *
   * @param string $value_callback
   *   The callback.
   */
  public function setValueCallback($value_callback) {
    $this->valueCallback = $value_callback;
  }

  /**
   * Returns the callback.
   *
   * @return string
   *   The callback.
   */
  public function getValueCallback() {
    return $this->valueCallback;
  }

  /**
   * Set the type used to determine the entity/bundle this field refers to.
   *
   * @param string $reference_type
   *   The entity/bundle type.
   */
  public function setReferenceType($reference_type) {
    $this->referenceType = $reference_type;
  }

  /**
   * Returns the entity/bundle type.
   *
   * @return string
   *   The entity/bundle type.
   */
  public function getReferenceType() {
    return $this->referenceType;
  }

  /**
   * Set whether or not this field should be exposed by the SDK.
   *
   * @param boolean $enabled
   *   Whether or not to expose this field.
   */
  public function setEnabled($enabled) {
    $this->enabled = $enabled;
  }

  /**
   * Returns whether or not to expose this field.
   *
   * @return boolean
   *   Whether or not to expose this field.
   */
  public function getEnabled() {
    return $this->enabled;
  }

  /**
   * Set whether or not the field is complex.
   *
   * @param boolean $complex
   *   Whether or not the field is complex.
   */
  public function setComplex($complex) {
    $this->complex = $complex;
  }

  /**
   * Returns whether or not the field is complex.
   *
   * @return boolean
   *   Whether or not the field is complex.
   */
  public function getComplex() {
    return $this->complex;
  }

  /**
   * Set the complex class.
   *
   * @param string $class
   *   The complex class.
   */
  public function setComplexClass($class) {
    $this->complexClass = $class;
  }

  /**
   * Returns the complex class.
   *
   * @return string
   *   The complex class.
   */
  public function getComplexClass() {
    return $this->complexClass;
  }
}
