<?php

/**
 * @file
 * This represents a foreign key reference to another entity. These are
 * determined in DrupalMetadataProvider and are created based on foreign keys
 * listed in hook_schema implementations.
 */

namespace ODataServer\Providers\Metadata;

class ForeignKey {

  /**
   * @var string
   */
  protected $key;

  /**
   * @var string
   */
  protected $entityName;

  /**
   * @var string
   */
  protected $bundleName;

  /**
   * @var bool
   */
  protected $enabled;

  /**
   * @var bool
   */
  protected $isBackReference;

  /**
   * Set the bundle this foerign key references.
   *
   * @param string $bundle_name
   *   The bundle this foerign key references.
   */
  public function setBundleName($bundle_name) {
    $this->bundleName = $bundle_name;
  }

  /**
   * Returns the bundle this foerign key references.
   *
   * @return string
   *   The bundle this foerign key references.
   */
  public function getBundleName() {
    return $this->bundleName;
  }

  /**
   * Set the entity this foreign key references.
   *
   * @param string $entity_name
   *   The entity this foreign key references.
   */
  public function setEntityName($entity_name) {
    $this->entityName = $entity_name;
  }

  /**
   * Returns the entity this foreign key references.
   *
   * @return string
   *   The entity this foreign key references.
   */
  public function getEntityName() {
    return $this->entityName;
  }

  /**
   * Set the property on the source entity used in the reference.
   *
   * @param string $key
   *   The property on the source entity used in the reference.
   */
  public function setKey($key) {
    $this->key = $key;
  }

  /**
   * Returns the property on the source entity used in the reference.
   *
   * @return string
   *   The property on the source entity used in the reference.
   */
  public function getKey() {
    return $this->key;
  }

  /**
   * Set whether to expose this foreign key through the SDK.
   *
   * @param boolean $enabled
   *   Whether to expose this foreign key through the SDK.
   */
  public function setEnabled($enabled) {
    $this->enabled = $enabled;
  }

  /**
   * Returns whether to expose this foreign key through the SDK.
   *
   * @return boolean
   *   Whether to expose this foreign key through the SDK.
   */
  public function getEnabled() {
    return $this->enabled;
  }

}
