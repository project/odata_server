<?php

/**
 * @file
 * Represents a property as defined in hook_schema implementations.
 */

namespace ODataServer\Providers\Metadata;

use Exception;

class Property {

  /**
   * @var string
   */
  protected $name;

  /**
   * @var string
   */
  protected $description;

  /**
   * @var string
   */
  protected $edmType;

  /**
   * @var string
   */
  protected $schemaType;

  /**
   * @var \callable
   */
  protected $valueCallback;

  /**
   * @var \callable
   */
  protected $expressionCallback;

  /**
   * @var bool
   */
  protected $enabled;

  /**
   * Set the property name.
   *
   * @param string $name
   *   The property name.
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * Returns the property name.
   *
   * @return string
   *   The property name.
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Set the EDM datatype.
   *
   * @param string $type
   *   The EDM datatype.
   */
  public function setEdmType($type) {
    $this->edmType = $type;
  }

  /**
   * Returns the EDM datatype.
   *
   * @return string
   *   The EDM datatype.
   */
  public function getEdmType() {
    return $this->edmType;
  }

  /**
   * Set the callback used to process the property value for the SDK.
   *
   * @param \callable $value_callback
   *   The callback.
   */
  public function setValueCallback($value_callback) {
    if (!is_callable($value_callback)) {
      throw new Exception('Invalid callback provided to setValueCallback: ' .
        (is_array($value_callback) ? join('::', $value_callback) : $value_callback));
    }
    $this->valueCallback = $value_callback;
  }

  /**
   * Returns the callback.
   *
   * @return \callable
   *   The callback.
   */
  public function getValueCallback() {
    return $this->valueCallback;
  }

  /**
   * Set whether to expose this property through the SDK.
   *
   * @param boolean $enabled
   *   Whether to expose this property through the SDK.
   */
  public function setEnabled($enabled) {
    $this->enabled = $enabled;
  }

  /**
   * Returns whether to expose this property through the SDK.
   *
   * @return boolean
   *   Whether to expose this property through the SDK.
   */
  public function getEnabled() {
    return $this->enabled;
  }

  /**
   * Set the description.
   *
   * @param string $description
   *   The description.
   */
  public function setDescription($description) {
    $this->description = $description;
  }

  /**
   * Returns the description.
   *
   * @return string
   *   The description.
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * Set the schema type.
   *
   * @param string $schema_type
   *   The schema type.
   */
  public function setSchemaType($schema_type) {
    $this->schemaType = $schema_type;
  }

  /**
   * Returns the schema type.
   *
   * @return string
   *   The schema type.
   */
  public function getSchemaType() {
    return $this->schemaType;
  }

  /**
   * Set the expression callback.
   *
   * @param \callable $expression_callback
   *   The expression callback.
   */
  public function setExpressionCallback($expression_callback) {
    if (!is_callable($expression_callback)) {
      throw new Exception('Invalid callback provided to setExpressionCallback: ' .
        (is_array($expression_callback) ? join('::', $expression_callback) : $expression_callback));
    }
    $this->expressionCallback = $expression_callback;
  }

  /**
   * Returns the expression callback.
   *
   * @return \callable
   *   The expression callback.
   */
  public function getExpressionCallback() {
    return $this->expressionCallback;
  }
}
