<?php

/**
 * @file
 * IExpressionProvider implementation used by the SDK to construct expressions.
 */

namespace ODataServer\Service;

use ODataProducer\UriProcessor\QueryProcessor\ExpressionParser\Expressions\ExpressionType;
use ODataProducer\Providers\Metadata\Type\IType;
use ODataProducer\Common\NotImplementedException;
use ODataProducer\Common\ODataConstants;
use ODataProducer\UriProcessor\QueryProcessor\ExpressionParser\IExpressionProvider;
use ODataProducer\UriProcessor\QueryProcessor\ExpressionParser\Expressions\PropertyAccessExpression;
use ODataProducer\UriProcessor\QueryProcessor\FunctionDescription\FunctionDescription;
use ODataProducer\Providers\Metadata\ResourceType;
use ODataServer\Service\DrupalMetadata;
use ODataServer\Providers\DrupalMetadataProvider;

class DrupalDSExpressionProvider implements IExpressionProvider {

  const ADD                  = '+';
  const CLOSE_BRACKET        = ')';
  const COMMA                = ',';
  const DIVIDE               = '/';
  const SUBTRACT             = '-';
  const EQUAL                = '=';
  const GREATERTHAN          = '>';
  const GREATERTHAN_OR_EQUAL = '>=';
  const LESSTHAN             = '<';
  const LESSTHAN_OR_EQUAL    = '<=';
  const LOGICAL_AND          = '&&';
  const LOGICAL_NOT          = '!';
  const LOGICAL_OR           = '||';
  const MEMBERACCESS         = '';
  const MODULO               = '%';
  const MULTIPLY             = '*';
  const NEGATE               = '-';
  const NOTEQUAL             = '!=';
  const OPEN_BRACKET          = '(';

  /**
   * The type of the resource pointed by the resource path segement
   *
   * @var ResourceType
   */
  protected $resourceType;

  /**
   * Get the name of the iterator.
   *
   * @return string
   *   The iterator name.
   */
  public function getIteratorName() {
    return NULL;
  }

  /**
   * Callback for setting the resource type.
   *
   * @param ResourceType $resource_type
   *   The resource type on which the filter is going to be applied.
   */
  public function setResourceType(ResourceType $resource_type) {
    $this->resourceType = $resource_type;
  }

  /**
   * Callback for logical expression.
   *
   * @param ExpressionType $expression_type
   *   The type of logical expression.
   * @param string $left
   *   The left expression.
   * @param string $right
   *   The left expression.
   *
   * @return mixed
   *   The expression
   */
  public function onLogicalExpression($expression_type, $left, $right) {
    switch ($expression_type) {
      case ExpressionType::AND_LOGICAL:
        return array(self::LOGICAL_AND, $left, $right);

      case ExpressionType::OR_LOGICAL:
        return array(self::LOGICAL_OR, $left, $right);

      default:
        throw new \InvalidArgumentException('onLogicalExpression');
    }
  }

  /**
   * Callback for arithmetic expression.
   *
   * @param ExpressionType $expression_type
   *   The type of arithmetic expression.
   * @param string $left
   *   The left expression.
   * @param string $right
   *   The left expression.
   *
   * @return mixed
   *   The expression.
   */
  public function onArithmeticExpression($expression_type, $left, $right) {
    switch ($expression_type) {
      case ExpressionType::MULTIPLY:
        return array(self::MULTIPLY, $left, $right);

      case ExpressionType::DIVIDE:
        return array(self::DIVIDE, $left, $right);

      case ExpressionType::MODULO:
        return array(self::MODULO, $left, $right);

      case ExpressionType::ADD:
        return array(self::ADD, $left, $right);

      case ExpressionType::SUBTRACT:
        return array(self::SUBTRACT, $left, $right);

      default:
        throw new \InvalidArgumentException('onArithmeticExpression');
    }
  }

  /**
   * Callback for relational expression.
   *
   * @param ExpressionType $expression_type
   *   The type of relation expression.
   * @param string $left
   *   The left expression.
   * @param string $right
   *   The left expression.
   *
   * @return mixed
   *   The expression.
   */
  public function onRelationalExpression($expression_type, $left, $right) {
    switch ($expression_type) {
      case ExpressionType::GREATERTHAN:
        return array(self::GREATERTHAN, $left, $right);

      case ExpressionType::GREATERTHAN_OR_EQUAL:
        return array(self::GREATERTHAN_OR_EQUAL, $left, $right);

      case ExpressionType::LESSTHAN:
        return array(self::LESSTHAN, $left, $right);

      case ExpressionType::LESSTHAN_OR_EQUAL:
        return array(self::LESSTHAN_OR_EQUAL, $left, $right);

      case ExpressionType::EQUAL:
        return array(self::EQUAL, $left, $right);

      case ExpressionType::NOTEQUAL:
        return array(self::NOTEQUAL, $left, $right);

      default:
        throw new \InvalidArgumentException('onArithmeticExpression');
    }
  }

  /**
   * Callback for unary expression.
   *
   * @param ExpressionType $expression_type
   *   The type of unary expression
   * @param string $child
   *   The child expression
   *
   * @return mixed
   *   The expression.
   */
  public function onUnaryExpression($expression_type, $child) {
    switch ($expression_type) {
      case ExpressionType::NEGATE:
        return array(self::NEGATE, $child);

      case ExpressionType::NOT_LOGICAL:
        return array(self::LOGICAL_NOT, $child);

      default:
        throw new \InvalidArgumentException('onUnaryExpression');
    }
  }

  /**
   * Callback for constant expression.
   *
   * @param IType $type
   *   The type of constant.
   * @param object $value
   *   The value of the constant.
   *
   * @return mixed
   *   The expression.
   */
  public function onConstantExpression(IType $type, $value) {
    if (is_bool($value)) {
      return var_export($value, TRUE);
    }
    elseif (is_null($value)) {
      return var_export(NULL, TRUE);
    }

    return $value;
  }

  /**
   * Callback for property access expression.
   *
   * @param PropertyAccessExpression $expression
   *   The property access expression
   *
   * @return mixed
   *   The expression.
   */
  public function onPropertyAccessExpression($expression) {
    $parent = $expression;
    $variable = NULL;
    $entity_type_name = $this->resourceType->getName();
    $property_name = $parent->getResourceProperty()->getName();

    return $property_name;
  }

  /**
   * Callback for function call expression.
   *
   * @param FunctionDescription $function_description
   *   Description of the function.
   * @param string[] $params
   *   Parameters to the function.
   *
   * @return mixed
   *   The expression.
   */
  public function onFunctionCallExpression($function_description, $params) {
    return func_get_args();
  }

  /**
   * Transforms expressions from DrupalDSExpressionProvider.
   *
   * @param array $expression
   *   The untransformed expression.
   * @param string $entity_type
   *   The entity type we're querying.
   * @param string $bundle_name
   *   The bundle we're querying.
   *
   * @return string
   *   The transformed, SQL-ready expression.
   *
   * @throws \InvalidArgumentException
   */
  public function buildWhereExpression($expression, $entity_type, $bundle_name) {
    $entity = DrupalMetadataProvider::getEntityByName($entity_type);
    $bundle = $entity->getBundleByName($bundle_name);
    $temp_expression = '';
    if (count($expression) == 2 && !is_string($expression[0])) {
      $description = $expression[0];
      $key = $expression[1][0];
      if ($property = $bundle->getPropertyByName($key)) {
        $callback = $property->getExpressionCallback();
        if ($callback) {
          $key = $callback($key);
        }
      }
      elseif ($field = $bundle->getFieldByName($key)) {
        $info = field_info_field($field->getName());
        $table = 'field_data_' . $field->getName();
        $key = array_shift($info['storage']['details']['sql'][FIELD_LOAD_CURRENT][$table]);
      }
      switch ($description->functionName) {
        case ODataConstants::STRFUN_COMPARE:
          $temp_expression = "STRCMP({$key}, {$expression[1][1]})";
          break;

        case ODataConstants::STRFUN_ENDSWITH:
          $temp_expression = "(STRCMP({$expression[1][1]},RIGHT({$key},LENGTH({$expression[1][1]}))) = 0)";
          break;

        case ODataConstants::STRFUN_INDEXOF:
          $temp_expression = "INSTR({$key}, {$expression[1][1]}) - 1";
          break;

        case ODataConstants::STRFUN_REPLACE:
          $temp_expression = "REPLACE({$key},{$expression[1][1]},{$expression[1][2]})";
          break;

        case ODataConstants::STRFUN_STARTSWITH:
          $temp_expression = "(STRCMP({$expression[1][1]},LEFT({$key},LENGTH({$expression[1][1]}))) = 0)";
          break;

        case ODataConstants::STRFUN_TOLOWER:
          $temp_expression = "LOWER({$key})";
          break;

        case ODataConstants::STRFUN_TOUPPER:
          $temp_expression = "UPPER({$key})";
          break;

        case ODataConstants::STRFUN_TRIM:
          $temp_expression = "TRIM({$key})";
          break;

        case ODataConstants::STRFUN_SUBSTRING:
          if (count($expression[1]) == 3) {
            $temp_expression = "SUBSTRING({$key}, {$expression[1][1]} + 1, {$expression[1][2]})";
          }
          else {
            $temp_expression = "SUBSTRING({$key}, {$expression[1][1]} + 1)";
          }
          break;

        case ODataConstants::STRFUN_SUBSTRINGOF:
          $temp_expression = "(LOCATE({$key}, {$expression[1][1]}) > 0)";
          break;

        case ODataConstants::STRFUN_CONCAT:
          $temp_expression = "CONCAT({$key},{$expression[1][1]})";
          break;

        case ODataConstants::STRFUN_LENGTH:
          $temp_expression = "LENGTH({$key})";
          break;

        case ODataConstants::GUIDFUN_EQUAL:
          $temp_expression = "STRCMP({$key}, {$expression[1][1]})";
          break;

        case ODataConstants::DATETIME_COMPARE:
          $temp_expression = "DATETIMECMP({$key}; {$expression[1][1]})";
          break;

        case ODataConstants::DATETIME_YEAR:
          $temp_expression = "EXTRACT(YEAR from {{$key}})";
          break;

        case ODataConstants::DATETIME_MONTH:
          $temp_expression = "EXTRACT(MONTH from {$key})";
          break;

        case ODataConstants::DATETIME_DAY:
          $temp_expression = "EXTRACT(DAY from {$key})";
          break;

        case ODataConstants::DATETIME_HOUR:
          $temp_expression = "EXTRACT(HOUR from {$key})";
          break;

        case ODataConstants::DATETIME_MINUTE:
          $temp_expression = "EXTRACT(MINUTE from {$key})";
          break;

        case ODataConstants::DATETIME_SECOND:
          $temp_expression = "EXTRACT(SECOND from {$key})";
          break;

        case ODataConstants::MATHFUN_ROUND:
          $temp_expression = "ROUND({$key})";
          break;

        case ODataConstants::MATHFUN_CEILING:
          $temp_expression = "CEIL({$key})";
          break;

        case ODataConstants::MATHFUN_FLOOR:
          $temp_expression = "FLOOR({$key})";
          break;

        case ODataConstants::BINFUL_EQUAL:
          $temp_expression = "({$key} = {$expression[1][1]})";
          break;

        case 'is_null':
          $temp_expression = "is_null({$key})";
          break;

        default:
          throw new \InvalidArgumentException('onFunctionCallExpression');
      }
    }
    elseif (count($expression) == 2 && is_string($expression[0])) {
      if (is_array($expression[1])) {
        $expression[1] = $this->buildWhereExpression($expression[1], $entity_type, $bundle_name);
      }
      $temp_expression = "{$expression[0]}({$expression[1]} {$expression[2]})";
    }
    elseif (count($expression) == 3) {
      if (is_array($expression[1])) {
        $expression[1] = $this->buildWhereExpression($expression[1], $entity_type, $bundle_name);
      }
      else {

        $key = $expression[1];
        if ($property = $bundle->getPropertyByName($key)) {
          $callback = $property->getExpressionCallback();
          if ($callback) {
            $key = $callback($key);
          }
        }
        elseif ($field = $bundle->getFieldByName($key)) {
          $info = field_info_field($field->getName());
          $table = 'field_data_' . $field->getName();
          $key = array_pop($info['storage']['details']['sql'][FIELD_LOAD_CURRENT][$table]);
        }
        $expression[1] = $key;
      }
      if (is_array($expression[2])) {
        $expression[2] = $this->buildWhereExpression($expression[2], $entity_type, $bundle_name);
      }
      $temp_expression = "({$expression[1]} {$expression[0]} {$expression[2]})";
    }
    return $temp_expression;
  }
}
