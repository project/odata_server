<?php

/**
 * @file
 * Wires up the IServiceProvider implementation.
 */

namespace ODataServer\Service;

use ODataProducer\Configuration\EntitySetRights;
use ODataProducer\DataService;
use ODataProducer\Configuration\DataServiceProtocolVersion;
use ODataProducer\Configuration\DataServiceConfiguration;
use ODataProducer\IServiceProvider;
use ODataProducer\OperationContext\DataServiceHost;
use ODataProducer\Common\ODataException;
use ODataProducer\Common\ODataConstants;
use ODataProducer\Common\Messages;
use ODataProducer\UriProcessor\UriProcessor;
use ODataServer\Service\DrupalMetadata;
use ODataServer\Service\DrupalQueryProvider;
use ODataServer\Service\DrupalDSExpressionProvider;

class DrupalDataService extends DataService implements IServiceProvider {

  protected $drupalMetadata = NULL;
  protected $drupalQueryProvider = NULL;
  protected $drupalExpressionProvider = NULL;

  /**
   * This method is called only once to initialize service-wide policies.
   *
   * @param DataServiceConfiguration &$config
   *   Data service configuration object
   */
  public function initializeService(DataServiceConfiguration &$config) {
    $config->setEntitySetPageSize('*', 5);
    $config->setEntitySetAccessRule('*', EntitySetRights::ALL);
    $config->setAcceptCountRequests(TRUE);
    $config->setAcceptProjectionRequests(TRUE);
    $config->setMaxDataServiceVersion(DataServiceProtocolVersion::V3);
    $config->setValidateETagHeader(FALSE);
  }

  /**
   * Get the service.
   *
   * @param String $service_type
   *   Type of service IDataServiceMetadataProvider, IDataServiceQueryProvider,
   *   IDataServiceStreamProvider
   *
   * @see library/ODataProducer/ODataProducer.IServiceProvider::getService()
   * @return object
   *   The service.
   */
  public function getService($service_type) {
    if (($service_type === 'IDataServiceMetadataProvider') ||
      ($service_type === 'IDataServiceQueryProvider2')) {
      if (is_null($this->drupalExpressionProvider)) {
        $this->drupalExpressionProvider = new DrupalDSExpressionProvider();
      }
    }
    if ($service_type === 'IDataServiceMetadataProvider') {
      if (is_null($this->drupalMetadata)) {
        $this->drupalMetadata = DrupalMetadata::create();
      }
      return $this->drupalMetadata;
    }
    elseif ($service_type === 'IDataServiceQueryProvider2') {
      if (is_null($this->drupalQueryProvider)) {
        $this->drupalQueryProvider = new DrupalQueryProvider();
      }
      return $this->drupalQueryProvider;
    }
    return NULL;
  }

}
