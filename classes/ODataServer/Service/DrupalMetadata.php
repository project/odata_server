<?php

/**
 * @file
 * Loads metadata classes into SDK.
 */

namespace ODataServer\Service;

use ODataProducer\Providers\Metadata\ResourceStreamInfo;
use ODataProducer\Providers\Metadata\ResourceAssociationSetEnd;
use ODataProducer\Providers\Metadata\ResourceAssociationSet;
use ODataProducer\Common\NotImplementedException;
use ODataProducer\Providers\Metadata\Type\EdmPrimitiveType;
use ODataProducer\Providers\Metadata\ResourceSet;
use ODataProducer\Providers\Metadata\ResourcePropertyKind;
use ODataProducer\Providers\Metadata\ResourceProperty;
use ODataProducer\Providers\Metadata\ResourceTypeKind;
use ODataProducer\Providers\Metadata\ResourceType;
use ODataProducer\Common\InvalidOperationException;
use ODataProducer\Providers\Metadata\IDataServiceMetadataProvider;
use ODataProducer\Providers\Metadata\ServiceBaseMetadata;
use ODataProducer\Providers\Metadata\MetadataMapping;
use ReflectionClass;
use ODataServer\Providers\DrupalMetadataProvider;

class DrupalMetadata {

  /**
   * Create metadata.
   *
   * @throws InvalidOperationException
   *
   * @return ServiceBaseMetadata
   *   The metadata class.
   */
  public static function create() {
    $edm = new ReflectionClass('\ODataProducer\Providers\Metadata\Type\EdmPrimitiveType');
    $metadata = new ServiceBaseMetadata('DrupalEntities', 'Drupal');
    $resource_set = array();
    $reference_properties = array();
    $back_reference_properties = array();
    $resource_types = array();
    $complex_types = array();
    foreach (DrupalMetadataProvider::getEntities() as $entity) {
      foreach ($entity->getBundles() as $bundle) {
        if ($bundle->getEnabled()) {
          $class = "\\ODataServer\\Entities\\{$entity->getName()}\\{$bundle->getName()}";
          $resource_type = $metadata->addEntityType(new ReflectionClass($class), ucfirst($entity->getName()) . '__' . ucfirst($bundle->getName()), 'Drupal');
          foreach ($bundle->getProperties() as $property) {
            // Primary key always enabled.
            if ($property->getEnabled() || $property->getName() == $entity->getPrimaryKey()) {
              $resource_types[$entity->getName() . "\\" . $bundle->getName()] = $resource_type;
              $type = explode('::', $property->getEdmType());
              $is_valid = TRUE;
              if (is_array($type) && count($type) !== 2) {
                $is_valid = FALSE;
              }
              if (!($type = $edm->getConstant($type[1]))) {
                $is_valid = FALSE;
              }
              if (!$is_valid) {
                throw new \Exception('Drupal schema properties must be a valid EdmPrimitiveType. "' . $property->getEdmType() . '" is not valid.');
              }
              if ($property->getName() == $entity->getPrimaryKey()) {
                $metadata->addKeyProperty($resource_type, $entity->getPrimaryKey(), $type);
              }
              else {
                $ref_class = new ReflectionClass('\ODataProducer\Providers\Metadata\Type\EdmPrimitiveType');

                $metadata->addPrimitiveProperty($resource_type, $property->getName(), $type);
              }
            }
          }
          foreach ($bundle->getFields() as $field) {
            if ($field->getEnabled()) {
              if ($field->getComplex()) {
                if (!isset($complex_types[$field->getComplexClass()])) {
                  $ref_class = new ReflectionClass($field->getComplexClass());
                  $complex_types[$field->getComplexClass()] = $metadata->addComplexType($ref_class, $field->getComplexClass(), 'Drupal', NULL);
                  $class = $field->getComplexClass();
                  $class = new $class();
                  foreach ($class->getProperties() as $complex_property_property => $complex_property_property_type) {
                    $ref_class = new ReflectionClass('\ODataProducer\Providers\Metadata\Type\EdmPrimitiveType');
                    $type = explode('::', $complex_property_property_type);
                    $metadata->addPrimitiveProperty($complex_types[$field->getComplexClass()], $complex_property_property, $ref_class->getConstant($type[1]));
                  }
                }
                $info = field_info_field($field->getName());
                $metadata->addComplexProperty($resource_type, $field->getName(), $complex_types[$field->getComplexClass()], $info['cardinality'] > 1 || $info['cardinality'] == -1);
              }
              else {
                if (!$field->getPrimitive()) {
                  $type = join("\\", explode('::', $field->getReferenceType()));
                  $_class = "\\ODataServer\\$type";
                  $ref_class = new ReflectionClass($_class);
                  $reference_properties[$_class::$entity_name . "\\" . $_class::$bundle_name][] = array(
                    'resourceType' => $resource_type,
                    'resourceKey' => $_class::$entity_name . "\\" . $_class::$bundle_name,
                    'propertyName' => $field->getName(),
                  );
                }
                $type = explode('::', $field->getEdmType());
                $ref_class = new ReflectionClass('\ODataProducer\Providers\Metadata\Type\EdmPrimitiveType');
                $info = field_info_field($field->getName());
                $metadata->addPrimitiveProperty($resource_type, $field->getName(), $ref_class->getConstant($type[1]), $info['cardinality'] > 1 || $info['cardinality'] == -1);
              }
            }
          }
          foreach ($bundle->getForeignKeys() as $foreign_key) {
            if ($foreign_key->getEnabled()) {
              $reference_properties[$entity->getName() . "\\" . $bundle->getName()][] = array(
                'resourceType' => $resource_type,
                'resourceKey' => $foreign_key->getEntityName() . "\\" . $foreign_key->getBundleName(),
                'propertyName' => $foreign_key->getKey(),
              );
            }
          }
          foreach ($bundle->getBackReferences() as $back_reference) {
            if ($back_reference->getEnabled()) {
              $back_reference_properties[$entity->getName() . "\\" . $bundle->getName()][] = $back_reference;
            }
          }
          $resource_set[$entity->getName() . "\\" . $bundle->getName()] = $metadata->addResourceSet(ucfirst($entity->getName()) . '__' . ucfirst($bundle->getName()), $resource_type);
        }
      }
    }
    foreach ($reference_properties as $property_collection) {
      foreach ($property_collection as $property) {
        if (isset($property['resourceType'])) {
          $resource_type = $property['resourceType'];
          if (isset($resource_set[$property['resourceKey']])) {
            $metadata->addResourceSetReferenceProperty($resource_type, join('__', explode("\\", $property['resourceKey'])) . '__' . $property['propertyName'], $resource_set[$property['resourceKey']]);
          }
        }
      }
    }
    foreach ($back_reference_properties as $back_reference_collection) {
      foreach ($back_reference_collection as $back_reference) {
        $resource_type = $resource_types[$back_reference->getSourceEntityName() . "\\" . $back_reference->getSourceBundleName()];
        $name = join('__', explode("\\", strtolower($back_reference->getTargetEntityName() . "\\" . $back_reference->getTargetBundleName()))) . '__' . $back_reference->getTargetForeignKey();
        $_resource_type = $back_reference->getTargetEntityName() . "\\" . $back_reference->getTargetBundleName();
        if (isset($resource_set[$_resource_type])) {
          $metadata->addResourceSetReferenceProperty($resource_type, $name, $resource_set[$_resource_type]);
        }
      }
    }

    return $metadata;
  }
}
