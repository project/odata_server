<?php

/**
 * @file
 * Handles OData requests which have been preprocessed by the SDK.
 */

namespace ODataServer\Service;

use ODataProducer\UriProcessor\ResourcePathProcessor\SegmentParser\KeyDescriptor;
use ODataProducer\Providers\Metadata\ResourceSet;
use ODataProducer\Providers\Metadata\ResourceProperty;
use ODataProducer\Providers\Query\IDataServiceQueryProvider2;
use ODataServer\Entities\DrupalEntityBase;
use ODataProducer\Common\ODataException;
use ODataServer\Entities\ODataEntityFieldQuery;
use ODataProducer\Common\InvalidOperationException;
use ODataProducer\UriProcessor\QueryProcessor\FunctionDescription\FunctionDescription;
use ODataServer\Service\DrupalMetadata;
use ODataServer\Providers\DrupalMetadataProvider;
use ODataProducer\Common\ODataConstants;

class DrupalQueryProvider implements IDataServiceQueryProvider2 {

  /**
   * Tell library to skip processing query options.
   *
   * @return Boolean
   *   True If user want library to apply the query options False If user is
   *   going to take care of orderby, skip and top options.
   */
  public function canApplyQueryOptions() {
    return FALSE;
  }

  /**
   * @var DrupalDSExpressionProvider
   */
  protected $drupalMySQLExpressionProvider;

  /**
   * Get the expression provider.
   */
  public function getExpressionProvider() {
    if (is_null($this->drupalMySQLExpressionProvider)) {
      $this->drupalMySQLExpressionProvider = new DrupalDSExpressionProvider();
    }
    return $this->drupalMySQLExpressionProvider;
  }

  /**
   * Gets collection of entities belongs to an entity set.
   *
   * @param ResourceSet $resource_set
   *   The entity set whose entities needs to be fetched.
   * @param string $filter
   *   Contains the filter condition.
   * @param string $select
   *   Contains the select condition.
   * @param string $orderby
   *   Contains the orderby condition.
   * @param string $top
   *   Contains the top condition.
   * @param string $skip
   *   Contains the skip condition.
   *
   * @return Object[]
   *   Returns a collection of entities matched by the request.
   */
  public function getResourceSet(
    ResourceSet $resource_set,
    $filter = NULL,
    $select = NULL,
    $orderby = NULL,
    $top = NULL,
    $skip = NULL
  ) {
    global $_odata_server_count;
    $info = explode('__', $resource_set->getName());
    $class = "\\ODataServer\\Entities\\" . join("\\", $info);
    if (!class_exists($class)) {
      ODataException::createNotImplementedError('Entity ' . $class . ' not found.');
    }
    $direction = 'ASC';
    $entity_type = strtolower($info[0]);
    $bundle = strtolower($info[1]);
    $properties = $class::getProperties();
    $query = new ODataEntityFieldQuery();
    if ($entity_type === 'taxonomy_term') {
      $vid = taxonomy_vocabulary_machine_name_load($bundle);
      $query
        ->entityCondition('entity_type', $entity_type)
        ->propertyCondition('vid', $vid->vid);
    }
    else if ($entity_type === 'user') {
      $query
        ->entityCondition('entity_type', $entity_type);
    }
    else {
      $query
        ->entityCondition('entity_type', $entity_type)
        ->entityCondition('bundle', $info[1]);
    }
    if ($skip) {
      $query->propertyCondition($class::getPrimaryKey(), $skip, $direction == 'ASC' ? '>' : '<');
    }
    if ($orderby) {
      foreach ($orderby->getOrderByPathSegments() as $segment) {
        foreach ($segment->getSubPathSegments() as $seg) {
          if (isset($properties['properties'][$seg->getName()])) {
            $query->propertyOrderBy($seg->getName(), $segment->isAscending() ? 'ASC' : 'DESC');
          }
          elseif (isset($properties['fields'][$seg->getName()])) {
            $info = field_info_field($seg->getName());
            if ($info['storage']['type'] === 'field_sql_storage' && $info['storage']['active'] == '1') {
              $field = $info['storage']['details']['sql'][FIELD_LOAD_CURRENT];
              $query->fieldOrderBy($seg->getName(), array_pop(array_keys(array_pop($field))), $segment->isAscending() ? 'ASC' : 'DESC');
            }
          }
        }
      }
    }
    if ($filter) {
      $expression = $this->drupalMySQLExpressionProvider->buildWhereExpression($filter, $entity_type, $bundle);
      $query->addWhereExpression($expression);
    }
    $count_query = clone $query;
    $_odata_server_count = $count_query->count()->execute();
    if ($top) {
      $query->range(0, $top);
    }

    $result = $query->execute();
    $entities = array();
    if (is_array($result) && isset($result[$entity_type])) {
      $entities = entity_load($entity_type, array_keys($result[$entity_type]));
    }
    $return_result = array();
    foreach ($entities as $entity) {
      $return_result[] = $this->serialize($entity, $class);
    }
    return $return_result;
  }

  /**
   * Gets an entity instance from an entity set identifed by a key.
   *
   * @param ResourceSet $resource_set
   *   The entity set from which an entity needs to be fetched.
   * @param KeyDescriptor $key_descriptor
   *   The key to identify the entity to be fetched
   *
   * @return NULL|Object
   *   Returns entity instance if found else null.
   */
  public function getResourceFromResourceSet(ResourceSet $resource_set, KeyDescriptor $key_descriptor) {
    $info = explode('__', $resource_set->getName());
    $class = "\\ODataServer\\Entities\\" . join("\\", $info);
    if (!class_exists($class)) {
      ODataException::createNotImplementedError('Entity ' . $class . ' not found.');
    }
    $entity_type = strtolower($info[0]);
    $bundle = strtolower($info[1]);
    $query = new ODataEntityFieldQuery();
    if ($entity_type === 'taxonomy_term') {
      $vid = taxonomy_vocabulary_machine_name_load($bundle);
      $query
        ->entityCondition('entity_type', $entity_type)
        ->propertyCondition('vid', $vid->vid);
    }
    else if ($entity_type === 'user') {
      $query
        ->entityCondition('entity_type', $entity_type);
    }
    else {
      $query
        ->entityCondition('entity_type', $entity_type)
        ->entityCondition('bundle', $info[1]);
    }
    $named_key_values = $key_descriptor->getValidatedNamedValues();
    foreach ($named_key_values as $key => $value) {
      $query->propertyCondition($key, $value[0]);
    }

    $result = $query->execute();
    $entities = array();
    if (is_array($result) && isset($result[$entity_type])) {
      $entities = entity_load($entity_type, array_keys($result[$entity_type]));
    }
    $return_result = array();
    foreach ($entities as $entity) {
      $return_result[] = $this->serialize($entity, $class);
    }
    return array_shift($return_result);
  }

  /**
   * Transform database object for use by SDK.
   *
   * @param object $row
   *   The database object.
   * @param DrupalEntityBase $class
   *   The metadata class.
   *
   * @return mixed
   *   The transformed object.
   */
  protected function serialize($row, $class) {
    $entity = new $class();
    $properties = $entity->getProperties();
    if (isset($properties['properties'])) {
      foreach ($properties['properties'] as $property_name => $property_type) {
        if (isset($row->$property_name)) {
          $property = DrupalMetadataProvider::getEntityByName($class::$entity_name)
            ->getBundleByName($class::$bundle_name)
            ->getPropertyByName($property_name);
          if ($property->getValueCallback()) {
            $entity->$property_name = call_user_func_array($property->getValueCallback(), array($row->$property_name));
          }
          else {
            $entity->$property_name = $row->$property_name;
          }
        }
      }
    }
    if (isset($properties['fields'])) {
      foreach ($properties['fields'] as $field_name => $field_type) {
        if (isset($row->$field_name)) {
          $field = DrupalMetadataProvider::getEntityByName($class::$entity_name)
            ->getBundleByName($class::$bundle_name)
            ->getFieldByName($field_name);
          if ($field && isset($row->{$field_name}) && count($row->{$field_name}) > 0) {
            // @todo Handle other languages.
            $value = call_user_func_array($field->getValueCallback(), array($row->{$field_name}[LANGUAGE_NONE], $field->getCardinality()));
            if (is_array($value)) {
              $entity->$field_name = $value;
            }
            else {
              $entity->$field_name = $value;
            }
          }
        }
      }
    }
    return $entity;
  }

  /**
   * Get related resource set for a resource.
   *
   * @param ResourceSet $source_resource_set
   *   The source resource set.
   * @param mixed $source_entity_instance
   *   The resource.
   * @param ResourceSet $target_resource_set
   *   The resource set of the navigation property.
   * @param ResourceProperty $target_property
   *   The navigation property to be retrieved.
   * @param string $filter
   *   Contains the filter condition.
   * @param string $select
   *   Contains the select condition.
   * @param string $orderby
   *   Contains the orderby condition.
   * @param string $top
   *   Contains the top condition.
   * @param string $skip
   *   Contains the skip condition.
   *
   * @return array|object[]
   *   Array of related resource if exists, if no related resources found
   *   returns empty array.
   */
  public function getRelatedResourceSet(
    ResourceSet $source_resource_set,
    $source_entity_instance,
    ResourceSet $target_resource_set,
    ResourceProperty $target_property,
    $filter = NULL,
    $select = NULL,
    $orderby = NULL,
    $top = NULL,
    $skip = NULL
  ) {
    global $_odata_server_count;
    $target_info = explode('__', $target_resource_set->getName());
    $target_class = "\\ODataServer\\Entities\\" . join("\\", $target_info);
    if (!class_exists($target_class)) {
      ODataException::createNotImplementedError('Entity ' . $target_class . ' not found.');
    }
    $target_entity_type = strtolower($target_info[0]);
    $target_bundle = strtolower($target_info[1]);
    $source_info = explode('__', $source_resource_set->getName());
    $source_entity_type = strtolower($source_info[0]);
    $source_bundle = strtolower($source_info[1]);

    if (!class_exists($target_class)) {
      ODataException::createNotImplementedError('Entity ' . $target_class . ' not found.');
    }
    $target_entity_type = strtolower($target_info[0]);
    $target_bundle = strtolower($target_info[1]);
    $properties = $target_class::getProperties();

    $query = new ODataEntityFieldQuery();
    if ($target_entity_type === 'taxonomy_term') {
      $vid = taxonomy_vocabulary_machine_name_load($target_bundle);
      $query
        ->entityCondition('entity_type', $target_entity_type)
        ->propertyCondition('vid', $vid->vid);
    }
    else if ($target_entity_type === 'user') {
      $query
        ->entityCondition('entity_type', $target_entity_type);
    }
    else {
      $query
        ->entityCondition('entity_type', $target_entity_type)
        ->entityCondition('bundle', $target_bundle);
    }
    $primary_key = $target_class::getPrimaryKey();

    $key = $source_entity_instance->{array_pop(explode('__', $target_property->getName()))};
    if ($key === FALSE || $key === NULL) {
      return array();
    }
    $key_name = $primary_key;

    $back_references = DrupalMetadataProvider::getEntityByName($source_entity_type)->getBundleByName($source_bundle)->getBackReferences();
    foreach ($back_references as $back_reference) {
      if ($back_reference->getEnabled()) {
        $key_name = DrupalMetadataProvider::getEntityByName($source_entity_type)->getPrimaryKey();
      }
    }
    $query->propertyCondition($key_name, $key);

    $direction = 'ASC';
    if ($skip) {
      $query->propertyCondition($target_class::getPrimaryKey(), $skip, $direction == 'ASC' ? '>' : '<');
    }
    if ($orderby) {
      foreach ($orderby->getOrderByPathSegments() as $segment) {
        foreach ($segment->getSubPathSegments() as $seg) {
          if (isset($properties['properties'][$seg->getName()])) {
            $query->propertyOrderBy($seg->getName(), $segment->isAscending() ? 'ASC' : 'DESC');
          }
          elseif (isset($properties['fields'][$seg->getName()])) {
            $info = field_info_field($seg->getName());
            if ($info['storage']['type'] === 'field_sql_storage' && $info['storage']['active'] == '1') {
              $field = $info['storage']['details']['sql'][FIELD_LOAD_CURRENT];
              $query->fieldOrderBy($seg->getName(), array_pop(array_keys(array_pop($field))), $segment->isAscending() ? 'ASC' : 'DESC');
            }
          }
        }
      }
    }
    if ($filter) {
      $query->addWhereExpression($this->drupalMySQLExpressionProvider->buildWhereExpression($filter, $target_entity_type, $target_bundle));
    }
    $count_query = clone $query;
    $_odata_server_count = $count_query->count()->execute();
    if ($top) {
      $query->range(0, $top);
    }

    $result = $query->execute();
    $entities = array();
    if (is_array($result) && isset($result[$target_entity_type])) {
      $entities = entity_load($target_entity_type, array_keys($result[$target_entity_type]));
    }
    $return_result = array();
    foreach ($entities as $entity) {
      $return_result[] = $this->serialize($entity, $target_class);
    }
    return $return_result;
  }

  /**
   * Gets a related entity instance from an entity set identifed by a key.
   *
   * @param ResourceSet $source_resource_set
   *   The entity set related to the entity to be fetched.
   * @param object $source_entity_instance
   *   The related entity instance.
   * @param ResourceSet $target_resource_set
   *   The entity set from which entity needs to be fetched.
   * @param ResourceProperty $target_property
   *   The metadata of the target property.
   * @param KeyDescriptor $key_descriptor
   *   The key to identify the entity to be fetched.
   *
   * @return NULL|object
   *   Returns entity instance if found else null.
   */
  public function getResourceFromRelatedResourceSet(
    ResourceSet $source_resource_set,
    $source_entity_instance,
    ResourceSet $target_resource_set,
    ResourceProperty $target_property,
    KeyDescriptor $key_descriptor
  ) {
    ODataException::createNotImplementedError('Unsupported query.');
  }
  /**
   * Get related resource for a resource.
   *
   * @param ResourceSet $source_resource_set
   *   The source resource set.
   * @param mixed $source_entity_instance
   *   The source resource.
   * @param ResourceSet $target_resource_set
   *   The resource set of the navigation property.
   * @param ResourceProperty $target_property
   *   The navigation property to be retrieved.
   *
   * @return NULL|object
   *   The related resource if exists else null.
   */
  public function getRelatedResourceReference(
    ResourceSet $source_resource_set,
    $source_entity_instance,
    ResourceSet $target_resource_set,
    ResourceProperty $target_property
  ) {
    ODataException::createNotImplementedError('Unsupported query.');
  }
}
