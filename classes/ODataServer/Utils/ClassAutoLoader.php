<?php

/**
 * @file
 * Sets up autoload paths for classes in the SDK and in the odata_server
 * module.
 */

namespace ODataServer\Utils;

class ClassAutoLoader {

  const FILEEXTENSION = '.php';

  /**
   * @var ClassAutoLoader
   */
  protected static $classAutoLoader;

  /**
   * @var array
   */
  public $libraryPaths = array();

  /**
   * Instantiate the singleton instance.
   *
   * @param array $library_paths
   *   The paths to add to the include_path.
   */
  public function __construct($library_paths) {
    $this->libraryPaths = $library_paths;
  }


  /**
   * Register class loader callback.
   */
  public static function register($library_paths) {
    if (self::$classAutoLoader == NULL) {
      self::$classAutoLoader = new ClassAutoLoader($library_paths);
      foreach ($library_paths as $library_path) {
        set_include_path(get_include_path() . PATH_SEPARATOR . $library_path);
        spl_autoload_register(
          array(self::$classAutoLoader, 'autoLoad')
        );
      }
    }
  }

  /**
   * Unregister class loader callback.
   */
  public static function unRegister() {
    if (self::$classAutoLoader != NULL) {
      spl_autoload_unregister(
        array(self::$classAutoLoader, 'autoLoad')
      );
    }
  }

  /**
   * Callback for class autoloading in linux flavours.
   *
   * @param string $class_path
   *   Path of the class to load.
   */
  public function autoLoad($class_path) {
    $class_path = str_replace("\\", "/", $class_path);
    foreach ($this->libraryPaths as $library_path) {
      if (file_exists($library_path . '/' . $class_path . self::FILEEXTENSION)) {
        include_once $library_path . '/' . $class_path . self::FILEEXTENSION;
        return;
      }
    }
  }
}
