<?php

/**
 * @file
 * Implements singleton pattern.
 */

namespace ODataServer\Utils;

/**
 * http://tinyurl.com/5fmor2
 */
abstract class Singleton {

  /**
   * Instantiates instance of subclass.
   *
   * @static
   * @return static
   *   The subclass instance.
   */
  final public static function getInstance() {
    static $ao_instance = array();

    $called_class_name = get_called_class();

    if (!isset($ao_instance[$called_class_name])) {
      $ao_instance[$called_class_name] = new $called_class_name();
    }
    return $ao_instance[$called_class_name];
  }

  /**
   * Implemented to prevent illegal instance creation.
   */
  final public function __clone() {

  }

}
