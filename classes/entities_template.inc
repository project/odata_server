<?php

/**
 * @file
 * Template used to construct and then eval() metadata classes required
 * by the OData SDK.
 */

use \ODataServer\Providers\DrupalMetadataProvider;

/** @var $entities \ODataServer\Providers\Metadata\Entity[] */
?>
<?php foreach ($entities as $entity) : ?>
namespace ODataServer\Entities\<?php print $entity->getName(); ?> {

  use ODataServer\Entities\DrupalEntityBase;

<?php foreach ($entity->getBundles() as $bundle) : ?>
  class <?php print $bundle->getName(); ?> extends DrupalEntityBase {

    public static $base_table = '<?php print $entity->getBaseTable(); ?>';
    public static $primary_key = '<?php print $entity->getPrimaryKey(); ?>';
    public static $bundle_name = '<?php print $bundle->getName(); ?>';
    public static $entity_name = '<?php print $entity->getName(); ?>';

    /* Database Fields */

<?php foreach ($bundle->getProperties() as $property) : ?>
    // <?php print $property->getEdmType(); ?>

    public $<?php print $property->getName(); ?>;

<?php if ($foreign_key = $bundle->getForeignKeyByKey($property->getName())) : ?>
    // Reference
<?php
  $target_entity = DrupalMetadataProvider::getEntityByName($foreign_key->getEntityName());
  $target_bundle = $target_entity->getBundleByName($foreign_key->getBundleName());
?>
    public $<?php print $target_entity->getName() . '__' . $target_bundle->getName(); ?>__<?php print $property->getName(); ?>;

<?php endif; ?>
<?php endforeach; ?>
<?php $fields = $bundle->getFields(); ?>
<?php if (count($fields) > 0) : ?>
    /* Field API Fields */

<?php foreach ($fields as $field) : ?>
    // <?php print $field->getEdmType(); ?>

    public $<?php print $field->getName(); ?>;

<?php if (!$field->getPrimitive()) : ?>
    // Reference
<?php
  $class = explode('::', $field->getReferenceType());
  array_shift($class);
  $target_entity = DrupalMetadataProvider::getEntityByName(array_shift($class));
  $target_bundle = $target_entity->getBundleByName(array_shift($class));
?>
    public $<?php print $target_entity->getName() . '__' . $target_bundle->getName(); ?>__<?php print $field->getName(); ?>;

<?php endif; ?>
<?php endforeach; ?>
<?php endif; ?>
<?php foreach ($bundle->getBackReferences() as $back_reference) : ?>
    // Backreference
<?php
$target_entity = DrupalMetadataProvider::getEntityByName($back_reference->getTargetEntityName());
$target_bundle = $target_entity->getBundleByName($back_reference->getTargetBundleName());
?>
    public $<?php print $target_entity->getName() . '__' . $target_bundle->getName(); ?>__<?php print $back_reference->getTargetForeignKey(); ?>;

<?php endforeach; ?>
    public static $properties = array(
      'properties' => array(
<?php foreach ($bundle->getProperties() as $property) : ?>
        '<?php print $property->getName(); ?>' => '<?php print $property->getEdmType(); ?>',
<?php endforeach; ?>
      ),
<?php if (count($fields) > 0) : ?>
      'fields' => array(
<?php foreach ($fields as $field) : ?>
        '<?php print $field->getName(); ?>' => '<?php print $field->getEdmType(); ?>',
<?php endforeach; ?>
      ),
<?php endif; ?>
<?php $foreign_keys = $bundle->getForeignKeys(); ?>
<?php if (count($foreign_keys) > 0) : ?>
      'foreign_keys' => array(
<?php foreach ($foreign_keys as $foreign_key) : ?>
        '<?php print $foreign_key->getKey(); ?>' => array(
          'entity' => '<?php print $foreign_key->getEntityName(); ?>',
          'bundle' => '<?php print $foreign_key->getBundleName(); ?>',
        ),
<?php endforeach; ?>
      ),
<?php endif; ?>
    );

  }

<?php endforeach; ?>
}

<?php endforeach;
