<?php

/**
 * @file
 * Admin configuration page.
 */

use ODataServer\Providers\DrupalMetadataProvider;
use ODataServer\Utils\ClassAutoLoader;

/**
 * Builds the admin configuration form array.
 *
 * @param array $form
 *   The form.
 * @param array &$form_state
 *   The form state.
 *
 * @return array
 *   The form.
 */
function odata_server_settings($form, &$form_state) {
  $library = libraries_load('odata');
  if (!$library['installed']) {
    form_set_error('regenerate', 'Unable to load OData SDK.');
  }
  else {
    require_once drupal_get_path('module', 'odata_server') . '/classes/ODataServer/Utils/ClassAutoLoader.php';
    ClassAutoLoader::register(array(
        drupal_get_path('module', 'odata_server') . '/classes',
        $library['library path'] . '/library',
    ));
    $entities = DrupalMetadataProvider::getEntities();
    $form['permissions'] = array(
      '#type' => 'fieldset',
      '#title' => 'Permissions',
    );
    $form['permissions']['info'] = array(
      '#markup' => '<p>' . t('Pick which bundles, properties, fields, and references should be exposed through the OData server.') . '</p>',
    );
    foreach ($entities as $entity) {
      $form['permissions'][$entity->getName()] = array(
        '#type' => 'fieldset',
        '#title' => $entity->getLabel(),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );
      $bundles = $entity->getBundles();
      if (count($bundles) > 0) {
        foreach ($bundles as $bundle) {
          $variable_name = 'odataserver-permissions-' . $entity->getName() . '__' . $bundle->getName();
          $form['permissions'][$entity->getName()]['bundles'][$bundle->getName()] = array(
            '#type' => 'fieldset',
            '#title' => $bundle->getName(),
            '#collapsible' => TRUE,
            '#collapsed' => TRUE,
          );
          $form['permissions'][$entity->getName()]['bundles'][$bundle->getName()][$variable_name] = array(
            '#type' => 'checkbox',
            '#title' => 'Expose through OData server',
            '#default_value' => variable_get($variable_name, FALSE),
          );
          $properties = $bundle->getProperties();
          if (count($properties) > 0) {
            $form['permissions'][$entity->getName()]['bundles'][$bundle->getName()]['properties'] = array(
              '#type' => 'fieldset',
              '#title' => 'Properties',
              '#collapsible' => TRUE,
              '#collapsed' => TRUE,
            );
            $headers = array(
              'name' => 'Property Name',
              'schemaType' => 'Schema Type',
              'edmType' => 'EDM Type',
              'description' => 'Description',
            );
            $variable_name = 'odataserver-permissions-' . $entity->getName() . '__' . $bundle->getName() . '::properties';
            $table = array(
              '#type' => 'tableselect',
              '#header' => $headers,
              '#default_value' => variable_get($variable_name, array()),
              '#options' => array(),
            );
            foreach ($properties as $property) {
              if ($entity->getPrimaryKey() !== $property->getName()) {
                $table['#options'][$property->getName()] = array(
                  'name' => $property->getName(),
                  'schemaType' => $property->getSchemaType(),
                  'edmType' => $property->getEdmType(),
                  'description' => $property->getDescription(),
                );
                if ($property->getName() === $entity->getPrimaryKey()) {
                  $table['#default_value'][$property->getName()] = TRUE;
                  $table['#options'][$property->getName()]['#disabled'] = TRUE;
                }
              }
            }
            $form['permissions'][$entity->getName()]['bundles'][$bundle->getName()]['properties'][$variable_name] = $table;
          }
          $foreign_keys = $bundle->getForeignKeys();
          if (count($foreign_keys) > 0) {
            $form['permissions'][$entity->getName()]['bundles'][$bundle->getName()]['foreign keys'] = array(
              '#type' => 'fieldset',
              '#title' => 'Foreign Keys',
              '#collapsible' => TRUE,
              '#collapsed' => TRUE,
            );
            $headers = array(
              'key' => 'Key',
              'targetEntity' => 'Target Entity',
            );
            $variable_name = 'odataserver-permissions-' . $entity->getName() . '__' . $bundle->getName() . '::foreign_keys';
            $table = array(
              '#type' => 'tableselect',
              '#header' => $headers,
              '#default_value' => variable_get($variable_name, array()),
              '#options' => array(),
            );
            foreach ($foreign_keys as $foreign_key) {
              $table['#options'][$foreign_key->getKey()] = array(
                'key' => $foreign_key->getKey(),
                'targetEntity' => $foreign_key->getEntityName(),
              );
            }
            $form['permissions'][$entity->getName()]['bundles'][$bundle->getName()]['foreign keys'][$variable_name] = $table;
          }
          $fields = $bundle->getFields();
          if (count($fields) > 0) {
            $form['permissions'][$entity->getName()]['bundles'][$bundle->getName()]['fields'] = array(
              '#type' => 'fieldset',
              '#title' => 'Fields',
              '#collapsible' => TRUE,
              '#collapsed' => TRUE,
            );
            $headers = array(
              'name' => 'Field Name',
              'field_api_type' => 'FieldAPI Type',
            );
            $variable_name = 'odataserver-permissions-' . $entity->getName() . '__' . $bundle->getName() . '::fields';
            $table = array(
              '#type' => 'tableselect',
              '#header' => $headers,
              '#default_value' => variable_get($variable_name, array()),
              '#options' => array(),
            );
            foreach ($fields as $field) {
              $table['#options'][$field->getName()] = array(
                'name' => $field->getName(),
                'field_api_type' => $field->getDrupalType(),
              );
            }
            $form['permissions'][$entity->getName()]['bundles'][$bundle->getName()]['fields'][$variable_name] = $table;
          }
        }
      }
    }
  }
  return system_settings_form($form);
}
