<?php

/**
 * @file
 * Default hook_odata_field_info implementation and associated callbacks.
 */

use \ODataServer\Providers\Metadata\Field;

/**
 * The default hook_odata_field_info implementation.
 *
 * @return array
 *   The field info array.
 */
function odata_server_odata_field_info() {
  $info = array();
  $info['text'] = array(
    'types' => array(
      'text' => array(
        'primitive' => TRUE,
        'type' => 'EdmPrimitiveType::STRING',
        'value callback' => 'odata_server_text_field_text_value',
      ),
      'text_with_summary' => array(
        'primitive' => TRUE,
        'type' => 'EdmPrimitiveType::STRING',
        'value callback' => 'odata_server_text_field_text_with_summary_value',
      ),
      'text_long' => array(
        'primitive' => TRUE,
        'type' => 'EdmPrimitiveType::STRING',
        'value callback' => 'odata_server_text_field_text_long_value',
      ),
    ),
  );
  $info['number'] = array(
    'types' => array(
      'number_integer' => array(
        'primitive' => TRUE,
        'type' => 'EdmPrimitiveType::INT32',
        'value callback' => 'odata_server_number_field_number_integer_value',
      ),
      'number_decimal' => array(
        'primitive' => TRUE,
        'type' => 'EdmPrimitiveType::DECIMAL',
        'value callback' => 'odata_server_number_field_number_decimal_value',
      ),
      'number_float' => array(
        'primitive' => TRUE,
        'type' => 'EdmPrimitiveType::DOUBLE',
        'value callback' => 'odata_server_number_field_number_float_value',
      ),
    ),
  );
  $info['list'] = array(
    'types' => array(
      'list_integer' => array(
        'primitive' => TRUE,
        'type' => 'EdmPrimitiveType::INT32',
        'value callback' => 'odata_server_list_field_list_integer_value',
      ),
      'list_float' => array(
        'primitive' => TRUE,
        'type' => 'EdmPrimitiveType::DOUBLE',
        'value callback' => 'odata_server_list_field_list_float_value',
      ),
      'list_text' => array(
        'primitive' => TRUE,
        'type' => 'EdmPrimitiveType::STRING',
        'value callback' => 'odata_server_list_field_list_text_value',
      ),
      'list_boolean' => array(
        'primitive' => TRUE,
        'type' => 'EdmPrimitiveType::BOOLEAN',
        'value callback' => 'odata_server_list_field_list_boolean_value',
      ),
    ),
  );
  $info['date'] = array(
    'types' => array(
      'datetime' => array(
        'primitive' => TRUE,
        'type' => 'EdmPrimitiveType::DATETIME',
        'value callback' => 'odata_server_date_field_datetime_value',
        'settings callback' => 'odata_server_date_field_date_settings',
      ),
      'date' => array(
        'primitive' => TRUE,
        'type' => 'EdmPrimitiveType::DATETIME',
        'value callback' => 'odata_server_date_field_datetime_value',
        'settings callback' => 'odata_server_date_field_date_settings',
      ),
      'datestamp' => array(
        'primitive' => TRUE,
        'type' => 'EdmPrimitiveType::DATETIME',
        'value callback' => 'odata_server_date_field_datestamp_value',
        'settings callback' => 'odata_server_date_field_datestamp_settings',
      ),
    ),
  );
  $info['taxonomy'] = array(
    'types' => array(
      'taxonomy_term_reference' => array(
        'primitive' => FALSE,
        'type' => 'Entities::taxonomy_term::$bundle',
        'value callback' => 'odata_server_taxonomy_field_taxonomy_term_reference_value',
        'settings callback' => 'odata_server_taxonomy_field_taxonomy_term_reference_settings',
      ),
    ),
  );
  $info['file'] = array(
    'types' => array(
      'file' => array(
        'primitive' => FALSE,
        'type' => 'Entities::file::$bundle',
        'value callback' => 'odata_server_file_field_file_reference_value',
        'settings callback' => 'odata_server_file_field_file_reference_settings',
      ),
    ),
  );
  $info['image'] = array(
    'types' => array(
      'image' => array(
        'primitive' => FALSE,
        'type' => 'Entities::file::$bundle',
        'value callback' => 'odata_server_file_field_file_reference_value',
        'settings callback' => 'odata_server_file_field_file_reference_settings',
      ),
    ),
  );
  $info['entityreference'] = array(
    'types' => array(
      'entityreference' => array(
        'primitive' => FALSE,
        'type' => 'Entities::$entity::$bundle',
        'value callback' => 'odata_server_entityreference_field_entityreference_value',
        'settings callback' => 'odata_server_entityreference_field_entityreference_settings',
      ),
    ),
  );
  $info['link'] = array(
    'types' => array(
      'link_field' => array(
        'primitive' => TRUE,
        'type' => 'EdmPrimitiveType::STRING',
        'value callback' => 'odata_server_link_field_link_value',
      ),
    ),
  );
  return $info;
}

/**
 * Transforms values for the SDK.
 *
 * @param array $items
 *   Array of values to transform for the SDK.
 * @param int $cardinality
 *   The cardinality of the field instance.
 *
 * @return mixed
 *   The transformed value(s).
 */
function odata_server_text_field_text_value($items, $cardinality = 1) {
  if ($cardinality != 1) {
    $return = array();
    foreach ($items as $item) {
      $return[] = $item['safe_value'];
    }
    return $return;
  }
  return $items[0]['safe_value'];
}

/**
 * Transforms values for the SDK.
 *
 * @param array $items
 *   Array of values to transform for the SDK.
 * @param int $cardinality
 *   The cardinality of the field instance.
 *
 * @return mixed
 *   The transformed value(s).
 */
function odata_server_text_field_text_with_summary_value($items, $cardinality = 1) {
  if ($cardinality != 1) {
    $return = array();
    foreach ($items as $item) {
      $return[] = $item['safe_value'];
    }
    return $return;
  }
  return $items[0]['safe_value'];
}

/**
 * Transforms values for the SDK.
 *
 * @param array $items
 *   Array of values to transform for the SDK.
 * @param int $cardinality
 *   The cardinality of the field instance.
 *
 * @return mixed
 *   The transformed value(s).
 */
function odata_server_text_field_text_long_value($items, $cardinality = 1) {
  if ($cardinality != 1) {
    $return = array();
    foreach ($items as $item) {
      $return[] = $item['safe_value'];
    }
    return $return;
  }
  return $items[0]['safe_value'];
}

/**
 * Transforms values for the SDK.
 *
 * @param array $items
 *   Array of values to transform for the SDK.
 * @param int $cardinality
 *   The cardinality of the field instance.
 *
 * @return mixed
 *   The transformed value(s).
 */
function odata_server_list_field_list_integer_value($items, $cardinality = 1) {
  if ($cardinality != 1) {
    $return = array();
    foreach ($items as $item) {
      $return[] = intval($item['value']);
    }
    return $return;
  }
  return intval($items[0]['value']);
}

/**
 * Transforms values for the SDK.
 *
 * @param array $items
 *   Array of values to transform for the SDK.
 * @param int $cardinality
 *   The cardinality of the field instance.
 *
 * @return mixed
 *   The transformed value(s).
 */
function odata_server_list_field_list_float_value($items, $cardinality = 1) {
  if ($cardinality != 1) {
    $return = array();
    foreach ($items as $item) {
      $return[] = floatval($item['value']);
    }
    return $return;
  }
  return floatval($items[0]['value']);
}

/**
 * Transforms values for the SDK.
 *
 * @param array $items
 *   Array of values to transform for the SDK.
 * @param int $cardinality
 *   The cardinality of the field instance.
 *
 * @return mixed
 *   The transformed value(s).
 */
function odata_server_list_field_list_text_value($items, $cardinality = 1) {
  if ($cardinality != 1) {
    $return = array();
    foreach ($items as $item) {
      $return[] = $item['value'];
    }
    return $return;
  }
  return $items[0]['value'];
}

/**
 * Transforms values for the SDK.
 *
 * @param array $items
 *   Array of values to transform for the SDK.
 * @param int $cardinality
 *   The cardinality of the field instance.
 *
 * @return mixed
 *   The transformed value(s).
 */
function odata_server_list_field_list_boolean_value($items, $cardinality = 1) {
  if ($cardinality != 1) {
    $return = array();
    foreach ($items as $item) {
      $return[] = (bool) $item['value'];
    }
    return $return;
  }
  return (bool) $items[0]['value'];
}

/**
 * Transforms values for the SDK.
 *
 * @param array $items
 *   Array of values to transform for the SDK.
 * @param int $cardinality
 *   The cardinality of the field instance.
 *
 * @return mixed
 *   The transformed value(s).
 */
function odata_server_number_field_number_integer_value($items, $cardinality = 1) {
  if ($cardinality != 1) {
    $return = array();
    foreach ($items as $item) {
      $return[] = intval($item['value']);
    }
    return $return;
  }
  return intval($items[0]['value']);
}

/**
 * Transforms values for the SDK.
 *
 * @param array $items
 *   Array of values to transform for the SDK.
 * @param int $cardinality
 *   The cardinality of the field instance.
 *
 * @return mixed
 *   The transformed value(s).
 */
function odata_server_number_field_number_decimal_value($items, $cardinality = 1) {
  if ($cardinality != 1) {
    $return = array();
    foreach ($items as $item) {
      $return[] = floatval($item['value']);
    }
    return $return;
  }
  return floatval($items[0]['value']);
}

/**
 * Transforms values for the SDK.
 *
 * @param array $items
 *   Array of values to transform for the SDK.
 * @param int $cardinality
 *   The cardinality of the field instance.
 *
 * @return mixed
 *   The transformed value(s).
 */
function odata_server_number_field_number_float_value($items, $cardinality = 1) {
  if ($cardinality != 1) {
    $return = array();
    foreach ($items as $item) {
      $return[] = floatval($item['value']);
    }
    return $return;
  }
  return floatval($items[0]['value']);
}

/**
 * Transforms values for the SDK.
 *
 * @param array $items
 *   Array of values to transform for the SDK.
 * @param int $cardinality
 *   The cardinality of the field instance.
 *
 * @return mixed
 *   The transformed value(s).
 */
function odata_server_date_field_datetime_value($items, $cardinality = 1) {
  if ($cardinality != 1) {
    $return = array();
    foreach ($items as $item) {
      $return[] = $item['value'];
    }
    return $return;
  }
  return $items[0]['value'];
}

/**
 * Transforms values for the SDK.
 *
 * @param array $items
 *   Array of values to transform for the SDK.
 * @param int $cardinality
 *   The cardinality of the field instance.
 *
 * @return mixed
 *   The transformed value(s).
 */
function odata_server_date_field_datestamp_value($items, $cardinality = 1) {
  if ($cardinality != 1) {
    $return = array();
    foreach ($items as $item) {
      if (intval($item['value']) == 0) {
        $return[] = NULL;
      }
      else {
        $datetime = new DateTime();
        $datetime->setTimestamp($item['value']);
        $return[] = $datetime->format('c');
      }
    }
    return $return;
  }
  if (intval($items[0]['value']) == 0) {
    $return[] = NULL;
  }
  $datetime = new DateTime();
  $datetime->setTimestamp($items[0]['value']);
  return $datetime->format('c');
}

/**
 * Settings callback.
 *
 * @param Field $field
 *   The field instance to configure.
 */
function odata_server_date_field_date_settings(Field $field) {
  $info = field_info_field($field->getName());
  if ($info['settings']['todate'] !== '') {
    $field->setComplex(TRUE);
    $field->setComplexClass('ComplexDate');
    $field->setValueCallback(array('ComplexDate', 'dateValueCallback'));
  }
}

/**
 * Settings callback.
 *
 * @param Field $field
 *   The field instance to configure.
 */
function odata_server_date_field_datestamp_settings(Field $field) {
  $info = field_info_field($field->getName());
  if ($info['settings']['todate'] !== '') {
    $field->setComplex(TRUE);
    $field->setComplexClass('ODataServer\ObjectModel\Date');
    $field->setValueCallback(array('ODataServer\ObjectModel\Date', 'datestampValueCallback'));
  }
}

/**
 * Transforms values for the SDK.
 *
 * @param array $items
 *   Array of values to transform for the SDK.
 * @param int $cardinality
 *   The cardinality of the field instance.
 *
 * @return mixed
 *   The transformed value(s).
 */
function odata_server_taxonomy_field_taxonomy_term_reference_value($items, $cardinality = 1) {
  if ($cardinality != 1) {
    $return = array();
    foreach ($items as $item) {
      $return[] = $item['tid'];
    }
    return $return;
  }
  return $items[0]['tid'];
}

/**
 * Settings callback.
 *
 * @param Field $field
 *   The field instance to configure.
 */
function odata_server_taxonomy_field_taxonomy_term_reference_settings(Field $field) {
  $info = field_info_field($field->getName());
  $vocabulary = array_pop($info['settings']['allowed_values']);
  $bundle = $vocabulary['vocabulary'];
  $field->setReferenceType(str_replace('$bundle', $bundle, $field->getEdmType()));
  $field->setEdmType('EdmPrimitiveType::INT32');
}

/**
 * Transforms values for the SDK.
 *
 * @param array $items
 *   Array of values to transform for the SDK.
 * @param int $cardinality
 *   The cardinality of the field instance.
 *
 * @return mixed
 *   The transformed value(s).
 */
function odata_server_file_field_file_reference_value($items, $cardinality = 1) {
  if ($cardinality != 1) {
    $return = array();
    foreach ($items as $item) {
      $return[] = $item['fid'];
    }
    return $return;
  }
  return $items[0]['fid'];
}

/**
 * Settings callback.
 *
 * @param Field $field
 *   The field instance to configure.
 */
function odata_server_file_field_file_reference_settings(Field $field) {
  $bundle = 'file';
  $field->setReferenceType(str_replace('$bundle', $bundle, $field->getEdmType()));
  $field->setEdmType('EdmPrimitiveType::INT32');
}

/**
 * Transforms values for the SDK.
 *
 * @param array $items
 *   Array of values to transform for the SDK.
 * @param int $cardinality
 *   The cardinality of the field instance.
 *
 * @return mixed
 *   The transformed value(s).
 */
function odata_server_entityreference_field_entityreference_value($items, $cardinality = 1) {
  if ($cardinality != 1) {
    $return = array();
    foreach ($items as $item) {
      $return[] = $item['target_id'];
    }
    return $return;
  }
  return $items[0]['target_id'];
}

/**
 * Settings callback.
 *
 * @param Field $field
 *   The field instance to configure.
 */
function odata_server_entityreference_field_entityreference_settings(Field $field) {
  $info = field_info_field($field->getName());
  $entity = $info['settings']['target_type'];
  $bundle = array_pop($info['settings']['handler_settings']['target_bundles']);
  $type = $field->getEdmType();
  $type = str_replace('$entity', $entity, $type);
  $type = str_replace('$bundle', $bundle, $type);
  $field->setReferenceType($type);
  $field->setEdmType('EdmPrimitiveType::INT32');
}

/**
 * Settings callback.
 *
 * @param array $items
 *   Array of values to transform for the SDK.
 * @param int $cardinality
 *   The cardinality of the field instance.
 */
function odata_server_link_field_link_value($items, $cardinality = 1) {
  if ($cardinality != 1) {
    $return = array();
    foreach ($items as $item) {
      $return[] = $item['url'];
    }
    return $return;
  }
  return $items[0]['url'];
}
