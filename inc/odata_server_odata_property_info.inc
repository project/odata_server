<?php

/**
 * @file
 * Default hook_odata_property_info implementation and associated callbacks.
 */

/**
 * The default hook_odata_property_info implementation.
 *
 * @return array
 *   The property info array.
 */
function odata_server_odata_property_info() {
  $info = array();
  $info['node'] = array(
    'status' => array(
      'type' => 'EdmPrimitiveType::BOOLEAN',
      'value callback' => 'odata_server_date_property_boolean_value',
    ),
    'promote' => array(
      'type' => 'EdmPrimitiveType::BOOLEAN',
      'value callback' => 'odata_server_date_property_boolean_value',
    ),
    'sticky' => array(
      'type' => 'EdmPrimitiveType::BOOLEAN',
      'value callback' => 'odata_server_date_property_boolean_value',
    ),
    'created' => array(
      'type' => 'EdmPrimitiveType::DATETIME',
      'value callback' => 'odata_server_date_property_datetime_value',
      'expression callback' => 'odata_server_date_property_expression_callback',
    ),
    'changed' => array(
      'type' => 'EdmPrimitiveType::DATETIME',
      'value callback' => 'odata_server_date_property_datetime_value',
      'expression callback' => 'odata_server_date_property_expression_callback',
    ),
  );
  $info['user'] = array(
    'created' => array(
      'type' => 'EdmPrimitiveType::DATETIME',
      'value callback' => 'odata_server_date_property_datetime_value',
      'expression callback' => 'odata_server_date_property_expression_callback',
    ),
    'access' => array(
      'type' => 'EdmPrimitiveType::DATETIME',
      'value callback' => 'odata_server_date_property_datetime_value',
      'expression callback' => 'odata_server_date_property_expression_callback',
    ),
    'login' => array(
      'type' => 'EdmPrimitiveType::DATETIME',
      'value callback' => 'odata_server_date_property_datetime_value',
      'expression callback' => 'odata_server_date_property_expression_callback',
    ),
    'pass' => array(
      'type' => 'EdmPrimitiveType::STRING',
      'description' => 'Exposing this property is potentially dangerous and should only be done after properly securing the OData service.',
    ),
    'data' => array(
      'type' => 'EdmPrimitiveType::BINARY',
      'description' => 'Exposing this property is potentially dangerous and should only be done after properly securing the OData service.',
    ),
    'status' => array(
      'type' => 'EdmPrimitiveType::BOOLEAN',
    ),
  );
  $info['file'] = array(
    'uri' => array(
      'type' => 'EdmPrimitiveType::STRING',
      'value callback' => 'odata_server_file_property_uri_callback',
    ),
  );
  return $info;
}

/**
 * Transforms values for the SDK.
 *
 * @param mixed $value
 *   Array of values to transform for the SDK.
 *
 * @return mixed
 *   The transformed value.
 */
function odata_server_date_property_datetime_value($value) {
  if (intval($value) == 0) {
    return NULL;
  }
  $datetime = new DateTime();
  $datetime->setTimestamp($value);
  return $datetime->format('c');
}

/**
 * Transforms values for the SDK.
 *
 * @param mixed $value
 *   Array of values to transform for the SDK.
 *
 * @return mixed
 *   The transformed value.
 */
function odata_server_date_property_boolean_value($value) {
  return (bool) $value;
}

/**
 * Transforms values for the SDK.
 *
 * @param mixed $value
 *   Array of values to transform for the SDK.
 *
 * @return mixed
 *   The transformed value.
 */
function odata_server_date_property_string_value($value) {
  return (string) $value;
}

/**
 * Transforms values for the SDK.
 *
 * @param mixed $value
 *   Array of values to transform for the SDK.
 *
 * @return mixed
 *   The transformed value.
 */
function odata_server_file_property_uri_callback($value) {
  return file_create_url($value);
}

/**
 * Transforms a timestamp field to a datetime field for use in EXTRACT().
 *
 * @param string $key
 *   The untransformed key.
 *
 * @return array
 *   The transformed key.
 */
function odata_server_date_property_expression_callback($key) {
  return "FROM_UNIXTIME($key)";
}
